package com.mindslab.chatbot.hubot.main.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.mindslab.chatbot.hubot.R;

public class ChatViewHolder extends RecyclerView.ViewHolder {
    public TextView tvChat;

    public ChatViewHolder(@NonNull View itemView) {
        super(itemView);
        tvChat = itemView.findViewById(R.id.tvChat);
    }
}
