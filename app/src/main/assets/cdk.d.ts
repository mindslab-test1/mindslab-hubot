declare module "uuid" {
    /**
     * Fast UUID generator, RFC4122 version 4 compliant.
     * @author Jeff Ward (jcward.com).
     * @license MIT license
     * @link http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript/21963136#21963136
     **/
    export class UUID {
        constructor();
        private static sLut;
        private static lookup;
        /**
         * UUID initialize
         */
        static initialize(): void;
        /**
         * Base64 encode
         *
         * @param {number} number를 Base64 인코딩
         * @returns {string}
         */
        private static tripletToBase64;
        /**
         * Combine the three bytes into a single integer
         *
         * @param {Uint8Array} uint8 입력 Byte Array
         * @param {number} start 시작 위치
         * @param {number} end 종료 위치
         * @returns {string}
         */
        private static encodeChunk;
        /**
         * Takes a byte array and returns string
         *
         * @param {Uint8Array} uint8 String으로 변환할 Byte Array
         * @returns {string}
         */
        private static fromByteArray;
        /**
         * Generate a Base64 to string
         *
         * @returns {string} Base64 인코딩된 값을 String으로 전달
         */
        static generateAsBase64(): string;
        /**
         * Generate a UUID to string
         *
         * @returns {string} UUID 값 생성
         */
        static generate(): string;
    }
}
declare module "proto" {
    global {
        function __assign(t: Object, ...objs: Object[]): Object;
    }
    /**
     * 빈 오브젝트 형태, VOID
     */
    export class Empty {
        constructor();
    }
    /**
     * 기간
     */
    export class Duration {
        /**
         * Signed seconds of the span of time. Must be from -315,576,000,000
         * to +315,576,000,000 inclusive.
         */
        seconds: number;
        /**
         * Signed fractions of a second at nanosecond resolution of the span
         * of time. Durations less than one second are represented with a 0
         * `seconds` field and a positive or negative `nanos` field. For durations
         * of one second or more, a non-zero value for the `nanos` field must be
         * of the same sign as the `seconds` field. Must be from -999,999,999
         * to +999,999,999 inclusive.
         */
        nanos: number;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * NOT USED, used as Date()
     */
    export class Timestamp {
        /**
         * Represents seconds of UTC time since Unix epoch
         * 1970-01-01T00:00:00Z. Must be from from 0001-01-01T00:00:00Z to
         * 9999-12-31T23:59:59Z inclusive.
         * int64 seconds = 1;
         */
        seconds: number;
        /**
         * Non-negative fractions of a second at nanosecond resolution. Negative
         * second values with fractions must still have non-negative nanos values
         * that count forward in time. Must be from 0 to 999,999,999
         * inclusive.
         */
        nanos: number;
        /**
         * 생성자,
         * 자동으로 현재의 시간을 구한다.
         */
        constructor();
        /**
         * 현재의 시간을 구한다.
         */
        getCurrentTimestamp(): void;
    }
    /**
     * 일반적인 비정형 데이터를 담을 수 있는 그릇입니다.
     */
    export class Struct {
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 입력 유형
     */
    export enum InputType {
        /**
         * STT를 통한 입력
         */
        SPEECH = "SPEECH",
        /**
         * 타이핑에 의한 입력
         */
        KEYBOARD = "KEYBOARD",
        /**
         * CARD 등에 지정된 텍스트를 보내는 경우
         */
        TOUTCH = "TOUTCH",
        /**
         * 이미지 인식의 결과를 보내는 경우, 이미지 애노테이션을 통한 처리
         */
        IMAGE = "IMAGE",
        /**
         *  OCR 이미지 인식의 결과를 보내는 경우
         */
        IMAGE_DOCUMENT = "IMAGE_DOCUMENT",
        /**
         * 이미지 인식의 결과를 보내는 경우
         */
        VIDEO = "VIDEO",
        /**
         * 세션을 생성하는 등의 최초 이벤트, 프로그램 등의 명시적인 이벤트로 처리하는 방식
         */
        OPEN_EVENT = "OPEN_EVENT"
    }
    /**
     * 사용자의 입력 데이터
     * 입력의 유형은 매우 다양할 수 있습니다.
     */
    export class Utter {
        /**
         * 사용자의 입력 메시지
         */
        utter: string;
        /**
         * 입력 유형
         */
        inputType: InputType;
        /**
         * 언어
         */
        lang: Lang;
        /**
         * 현재의 발화에 대한 추가적인 메시지가 존재할 수 있습니다.
         * ETRI STT의 경우에는 이 메시지가 없습니다.
         */
        altUtters: string[];
        /**
         * 추가적인 입력 데이터
         * IMAGE, IMAGE_DOCUMENT. VIDEO, EVENT의 경우
         * 추가적인 데이터 메시지 데이터를 정의해서 보낼 수 있습니다.
         * 이미지 문서 인식의 경우에는 JSON 형태의 meta 데이터를
         * 여기에 담아서 넣어야 합니다.
         */
        meta: Struct;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 명시적으로 새로운 챗봇을 엽니다. 새로운 대화 세션을 생성합니다.
     */
    export class OpenUtter {
        /**
         * 사용할 챗봇의 이름
         */
        chatbot: string;
        /**
         * 명시적으로 지정할 SKILL 이름 (이 스펙이 꼭 필요한가?)
         */
        skill: string;
        /**
         * 초기에 보내줄 발화
         * 기본적으로 빈 문자열이 필요하다.
         */
        utter: string;
        /**
         * 추가적인 입력 데이터
         * OPEN 시점에 대화 메시지 외에 추가할 데이터
         */
        meta: Struct;
        /**
         * 생성자
         */
        constructor();
    }
    export class Event {
        /**
         * Event interface
         */
        interface: string;
        /**
         * Event operation
         */
        operation: string;
        /**
         * DA에서 단말에게 전달되는 param
         */
        daForwardParam: DialogAgentForwarderParam;
        /**
         * Event payload 정의
         */
        eventPayload: EventPayload;
        constructor();
    }
    /**
     * Event 관련 payload
     */
    export class EventPayload {
        private launcherAuthorized;
        private launcherAuthorizeFailed;
        private launcherSlotsFilled;
        private audioPlayerPlay;
        private launcherDocumentRead;
        constructor();
        _launcherAuthorized: LauncherAuthorized;
        _launcherAuthorizeFailed: LauncherAuthorizeFailed;
        _launcherSlotsFilled: LauncherSlotsFilled;
        _audioPlayerPlay: AudioPlayerPlay;
        _launcherDocumentRead: LauncherDocumentRead;
    }
    /**
     * 단말의 처리 능력
     */
    export class Capability {
        /**
         * OUTPUT: 텍스트를 출력할 수 있다.
         */
        supportRenderText: boolean;
        /**
         * OUTPUT: 카드를 처리할 수 있다.
         */
        supportRenderCard: boolean;
        /**
         * OUTPUT: 음성합성을 처리할 수 있다.
         */
        supportSpeechSynthesizer: boolean;
        /**
         * OUTPUT: 오디오를 출력할 수 있다.
         */
        supportPlayAudio: boolean;
        /**
         * OUTPUT: 비디오를 출력할 수 있다.
         */
        supportPlayVideo: boolean;
        /**
         * OUTPUT: 행위를 처리할 수 있다.
         */
        supportAction: boolean;
        /**
         * OUTPUT: 소스를 움직일 수 있다.
         */
        supportMove: boolean;
        /**
         * INPUT: 음성입력 대기를 처리할 수 있다.
         */
        supportExpectSpeech: boolean;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 장치
     */
    export class Device {
        /**
         * 장치를 유일하게 식별하는 공유번호,
         * 장치에서 전송할 경우에는 반드시 서로 다른 일련번호를 가져야 합니다.
         * 모든 장치는 하나의 서비스에서 절대적으로 유일한 일련번호를 가지고 있어야 한다
         * 이 정보는 장치별 세션을 생성하는데 사용된다.
         *
         * 웹의 경우, 최초 접근하면 강제로 생성해주는 COOKIE 같은 개념
         * 서로 다른 PC와 브라우저마다 따로 존재해야 햔다.
         */
        id: string;
        /**
         * 장치의 타입은 문자열로 정의되면, 서비스에 따라서 매우 상이하게 정의할 수 있습니다.
         * 예시) 엡, 안드로이드, IOS, 봇, 스피커, PC. 카카오, 페이스북
         * 접근하는 클라이언트의 API 접속 방법을 분리하는 것이다.
         * 모델 개념을 합친다.
         */
        type: string;
        /**
         * *optional* 장치의 버전 정보, 장치의 SW나 MODEL과는 다른 개념입니다.
         */
        version: string;
        /**
         * 서비스 접근 경로
         * *optional* 사이트별로 추가적인 정보가 필요할 수도 있다.
         * 내부적인 업무 구분 용도로 사용될 수 있다.
         */
        channel: string;
        /**
         * 장치의 처리 능력
         *
         * input 능력
         *   mic, camera_image, camera_movie, keyboard, pointing
         * output 능력
         *   display_text, display_image, speaker, action, move, speaker
         */
        support: Capability;
        /**
         * *optional* 장치의 현재 시간 정보
         */
        timestamp: Date;
        /**
         * *optional* 장치의 timezone 정보
         */
        timezone: string;
        /**
         * *optional* 장치의 메타 데이터 정보
         */
        meta: Struct;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * DA에서 단말로 내려보내주는 PAYLOAD
     * 아바타 출력을 위한 페이로드
     */
    export class AvatarSetExpressionPayload {
        /**
         * 아바타 출력 장치 정의
         * target에 어느 장치에 출력을 할지 정의한다
         */
        target: string;
        /**
         * 화면에 표현데이터
         */
        expression: string;
        /**
         * 표현 데이터에 대한 설명
         */
        desciption: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 오디오 출력을 위한 페이로드
     * 사용자에게 오디오 스트림또는 오디오 파일을 스피커로 출력하도록 지시한다.
     */
    export class AudioPlayPayload {
        stream: Stream;
        metadata: Metadata;
    }
    /**
     * 오디오 스트림의 정보
     */
    export class Stream {
        id: string;
        url: string;
        playTime: Duration;
        constructor();
    }
    /**
     * 오디오 스트림의 정보
     */
    export class Metadata {
        title: string;
        subtitle: string;
        art: Sources;
        backgroundImage: Sources;
        constructor();
    }
    /**
     * 부가 정보의 위치
     */
    export class Sources {
        urls: string[];
        constructor();
    }
    /**
     * 오디오 출력의 중지
     */
    export class AudioStopPayload {
        id: string;
        reason: string;
        constructor();
    }
    /**
     * 비디오 출력을 위한 페이로드
     */
    export class VideoPlayPayload {
        id: string;
        title: string;
        url: string;
        playTime: Duration;
        constructor();
    }
    /**
     * 지도표시를 위한 페이로드
     */
    export class LauncherViewMapPayload {
        /**
         * 검색범위, 반경, 반경 1km 이내 등과 같은 자연어 표현식
         */
        searchRange: string;
        /**
         * 검색 목적물: ATM, 지점
         */
        searchObject: string;
        /**
         * 검색어 : 영등포지점
         */
        searchKeyword: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 단말에 대화 처리에 대한 일부 데이터 위임을 하는 경우에 대한 처리
     * 모든 처리를 SKIP하기 위해서 최대한 상세한 정보를 넘겨주도록 한다.
     */
    /**
     * 인증 처리를 위한 페이로드
     */
    export class LauncherAuthorizePayload {
        /**
         * 사용할 인증 제공자
         */
        provider: string;
        /**
         * 인증 방법
         */
        method: string;
        /**
         * 인증을 위한 추가파라미터
         */
        meta: Struct;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 단말 정보 획득 위한 페이로드
     */
    export class LauncherFillSlotsPayload {
        /**
         * 채워진 슬롯 정보들
         */
        filledSlots: Struct;
        /**
         * 요청하는 슬롯 정보들
         */
        requestingSlots: string[];
        constructor();
    }
    /**
     * 문서를 읽은 결과 처리하기 위한 페이로드
     */
    export class LauncherReadDocumentPayload {
        /**
         * 문서 id
         */
        id: string;
        /**
         * 문서 url
         */
        documentUrl: string;
        /**
         * 문서 title
         */
        title: string;
        /**
         * 문서 sub_titile
         */
        subTitle: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 권한 부여 성공
     */
    export class LauncherAuthorized {
        /**
         * 성공한 accessToken
         * 이 정보는 참고성이다.
         * 헤더에 어떤 값이 들어있는지 정보를 획득하도록 한다.
         */
        accessToken: string;
        /**
         * 획득 시간 정보
         */
        authorizedAt: Date;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 권한 부여 실패
     */
    export class LauncherAuthorizeFailed {
        /**
         * 접근 토큰
         */
        accessToken: string;
        /**
         * 인증 시간
         */
        authorizedAt: Date;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 슬롯이 채워진 경우에 그 값을 정해준다.
     */
    export class LauncherSlotsFilled {
        /**
         * 채워진 슬롯
         */
        filledSlots: Struct;
        /**
         * 아직도 채워지지 않는 슬롯
         */
        unfilledSlots: string[];
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * AudioPlayerPlay 관련 payload 정보
     */
    export class AudioPlayerPlay {
        /**
         * 오디오 id
         */
        id: string;
        /**
         * 오디오 play 이벤트 (예:finish, stop, pause, resume)
         */
        reason: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 문서 읽기 유/무를 처리하기 위한 정보
     */
    export class LauncherDocumentRead {
        /**
         * 문서 id
         */
        id: string;
        /**
         * 해당 문서를 읽었으면 true, 읽지 않았으면 false (read는 과거분사형)
         */
        read: boolean;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 클라이언트에게 LaunchApp을 실행하는 경우
     * interface: Client
     * operation: LaunchApp
     */
    export class LauncherLaunchAppPayload {
        /**
         *
         */
        target: string;
        /**
         * 실행 대상의 내부 실행 정보
         */
        uri: string;
        /**
         * 실행 대상에게 전달되어야할 상세한 파라미터
         */
        meta: Struct;
        /**
         * 실행 환경 정보
         * OS, VERSION, 기타 정보
         */
        runtimeEnv: string;
        /**
         * 이 실행이 가지는 추가적인 설명 정보
         */
        description: string;
        constructor();
    }
    /**
     * 클라이언트에게 LaunchPage을 실행하는 경우
     * 주로 웹 브라우저인 M2U 단말이 실행을 가능성이 높습니다.
     *
     * interface: Client
     * operation: LaunchPage
     */
    export class LauncherLaunchPagePayload {
        /**
         *실행 대상 정보 : browser target
         */
        target: string;
        /**
         * 실행 대상의 내부 실행 정보
         */
        uri: string;
        /**
         * 실행 대상에게 전달되어야할 상세한 파라미터
         */
        meta: Struct;
        /**
         * 브라우저 환경 정보
         * 브라우저 VERSION, 기타 정보
         */
        browserEnv: string;
        /**
         * 이 페이지 실행이 가지는 추가적인 설명 정보
         */
        description: string;
        constructor();
    }
    /**
     * DA에서 단말로 내려보나는 경우에는 현재의 세션, SKILL, INENT를 모두 담아서 내려보내고
     * 이를 바로 받아볼 수 있도록 처리한다.
     */
    export class DialogAgentForwarderParam {
        /**
         *  챗봇 */
        chatbot: string;
        /**
         *  스킬 */
        skill: string;
        /**
         *  의도 */
        intent: string;
        /**
         *  세션 ID */
        sessionId: number;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 위경도를 포함한 위치 정보
     */
    export class Location {
        /** 위도 */
        latitude: number;
        /** 경도 */
        longitude: number;
        /**
         * 기타 문자열로 표현된 장소 정보, "경복궁", "경포대"
         * *optional*
         * 통상적인 경우는 비어있습니다.
         */
        location: string;
        /**
         * 현재 위치가 채집된 시간, 위치는 시간에 따라서 바뀔 수 있다.
         */
        refreshAt: Date;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 언어와 로캘을 함께 포함한 구조
     */
    export enum Lang {
        /**
         * 한국어, 한국
         */
        ko_KR = "ko_KR",
        /**
         * 영어 미국
         */
        en_US = "en_US"
    }
    /**
     * 이미지 포맷
     */
    export enum ImageFormat {
        /** JPEG */
        JPEG = "JPEG",
        /** PNG */
        PNG = "PNG",
        /** bitmap */
        BMP = "BMP",
        /** TIFF format */
        TIFF = "TIFF",
        /** pdf format */
        PDF = "PDF"
    }
    /**
     * Image Recognition Param 에 사용될 수 있는 파라미터.
     */
    export class ImageRecognitionParam {
        /**
         * 출력 인코딩 포맷
         */
        imageFormat: ImageFormat;
        /**
         * 출력 언어
         */
        lang: Lang;
        /**
         * guide 비율의 가로 값
         */
        refVertexX: number;
        /**
         * guide 비율의 세로 값
         */
        refVertexY: number;
        /**
         * 대칭형 crop (True), 비대칭형 crop (False)
         */
        symmCrop: boolean;
        /**
         * 너비
         */
        width: number;
        /**
         * 높이
         */
        height: number;
        /** 생성자 */
        constructor(init: Partial<ImageRecognitionParam>);
    }
    /**
     * 주의: Result로 표현되는 경우는 하나의 요청에 대한 응답으로만 동작하는 것이 아니라
     * 다른 곳에 재전송할 수 있는 데이터 유형일 때이다.
     */
    /**
     * 이미지 인식 결과물
     *
     */
    export class ImageRecognitionResult {
        /** 이미지 분류 표시 */
        imageClass: string;
        /** 인식된 결과물을 표현하는 텍스트 */
        annotatedTexts: string[];
        /** 인식된 이미지로부터 표현가능한 다양한 정보 */
        meta: Struct;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 음성인식을 위한 파라미터
     *
     * AudioEncoding, sample_rate 등의 옵션을 가지고 있다.
     */
    export class SpeechRecognitionParam {
        /**
         * STT encoding
         * 현재는 PCM만 지원한다.
         */
        encoding: AudioEncoding;
        /**
         * STT sample rate
         * 8K, 16K를 지원한다. 기본값으로 16K를 사용한다.
         */
        sampleRate: number;
        /**
         * STT Lang
         * 현재 언어는 kor, eng 만 사용가능하다.
         */
        lang: Lang;
        /**
         * STT 모델
         * *Optional* STT 학습 모델 정보는 지정된 내용으로 전송될 수 있다.
         */
        model: string;
        /**
         * if true, detect endpoint
         * *Optional* If `false` or omitted, the recognizer will perform continuous
         * recognition (continuing to wait for and process audio even if the user
         * pauses speaking) until the client closes the input stream (gRPC API) or
         * until the maximum time limit has been reached. May return multiple
         * `StreamingRecognitionResult`s with the `is_final` flag set to `true`.
         *
         * If `true`, the recognizer will detect a single spoken utterance. When it
         * detects that the user has paused or stopped speaking, it will return an
         * `END_OF_SINGLE_UTTERANCE` event and cease recognition. It will return no
         * more than one `StreamingRecognitionResult` with the `is_final` flag set to
         * `true`.
         */
        singleUtterance: boolean;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * Indicates the type of speech event.
     */
    export enum SpeechEventType {
        /** No speech event specified. */
        SPEECH_EVENT_UNSPECIFIED = "SPEECH_EVENT_UNSPECIFIED",
        /**
         * This event indicates that the server has detected the end of the user's
         * speech utterance and expects no additional speech. Therefore, the server
         * will not process additional audio (although it may subsequently return
         * additional results). The client should stop sending additional audio
         * data, half-close the gRPC connection, and wait for any additional results
         * until the server closes the gRPC connection. This event is only sent if
         * `single_utterance` was set to `true`, and is not used otherwise.
         */
        END_OF_SINGLE_UTTERANCE = "END_OF_SINGLE_UTTERANCE"
    }
    /**
     * 음성인식 응답
     *
     * `StreamingRecognizeResponse` is the only message returned to the client by
     * `StreamingRecognize`. A series of zero or more `StreamingRecognizeResponse`
     * messages are streamed back to the client. If there is no recognizable
     * audio, and `single_utterance` is set to false, then no messages are streamed
     * back to the client.
     * - Only two of the above responses #4 and #7 contain final results; they are
     * indicated by `is_final: true`. Concatenating these together generates the
     * full transcript: "to be or not to be that is the question".
     *
     * - The others contain interim `results`. #3 and #6 contain two interim
     * `results`: the first portion has a high stability and is less likely to
     * change; the second portion has a low stability and is very likely to
     * change. A UI designer might choose to show only high stability `results`.
     *
     * - The specific `stability` and `confidence` values shown above are only for
     * illustrative purposes. Actual values may vary.
     *
     * - In each response, only one of these fields will be set:
     *`error`,
     * `speech_event_type`, or
     * one or more (repeated) `results`.
     */
    export class StreamingRecognizeResponse {
        /**
         * *Output-only* If set, returns a [google.rpc.Status][google.rpc.Status] message that
         * specifies the error for the operation.
         * google.rpc.Status error = 1;
         * maum.ai에서는 NOT_OK인 경우에 detail_error_messages에서 해당 내용을 꺼내서 사용할 수 있도록 한다.
         * *Output-only* This repeated list contains zero or more results that
         * correspond to consecutive portions of the audio currently being processed.
         * It contains zero or more `is_final=false` results followed by zero or one
         * `is_final=true` result (the newly settled portion).
         */
        results: SpeechRecognitionResult[];
        /**
         * *Output-only* Indicates the type of speech event.
         */
        speechEventType: SpeechEventType;
        constructor();
    }
    /**
     * STT Result를 위한 타입
     * 단어 정보
     */
    export class WordInfo {
        /**
         * 단어 시작 시간
         */
        startTime: Duration;
        /**
         * 단어 끝나는 시간
         */
        endTime: Duration;
        /**
         * 단어
         */
        word: string;
        constructor();
    }
    /**
     * 음성인식 결과
     */
    export class SpeechRecognitionResult {
        /**
         * 인식된 전체
         */
        transcript: string;
        /**
         * 최종 여부
         */
        final: boolean;
        /**
         * 단어 단위로 시간 표시
         */
        words: WordInfo[];
        constructor();
    }
    /**
     * TTS 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
     */
    export class SpeechSynthesizerParam {
        /**
         * 출력 인코딩 포맷
         */
        encoding: AudioEncoding;
        /**
         * 출력 언어
         */
        lang: Lang;
        /**
         * 출력 PCM
         * *Optional*, MP3의 경우에는 꼭 필요하지는 않다.
         * 8K, 16K를 지원한다. 기본값으로 16K를 사용한다.
         */
        sampleRate: number;
        constructor();
    }
    /**
     *
     * 카드 형태의 데이터
     * 카드 형태의 데이터를 나타나도록 지시한다.
     * CHART
     *    type: 그래프 종류..
     *    options : 출력형태
     *    data: [] , 내부적으로 대시 배열 2차 3차 , object 로 동작한다
     * SELECT CARD
     *  title, 요약 사진 , selected utter,
     * NEWS LIST
     *  title, 요약, 사진 .. 링크 ...
     */
    /**
     * 차트 형태의 데이터
     * google charts 형식을 지원할 수 있도록 정의한다.
     */
    export class ChartCard {
        /**
         * 차트 타입:
         *  - "PieChart"
         *  - "BarChart"
         *  - "XXXChart"
         */
        type: string;
        /**
         * 각 차트별로 출력 가능한 다양한 옵션을 지정한다.
         */
        options: Struct;
        /**
         * 차트 출력을 위한 실제 대이터는 배렬로 제공된다.
         * 각각의 ListValue는 array로 표현된다.
         * ListValue 내부에는 다시 ListValue를 가질 수 있고 다차원으료 표현이 가능한다.
         */
        data: any[];
        constructor();
    }
    /**
     * Grid 형태의 데이터
     */
    export class GridCard {
        /**
         * Grid type에는 데이터 형식에 대한 정보를 제공한다
         * - 시장금리 추이
         * - 기준금리 추이
         * - 대출금리 추이
         * 예를 들어 type이 '시장금리 추이' 라면 해당 표는 금리추세를 표현한 표임을 알 수 있다.
         */
        type: string;
        /**
         * 표 출력을 위한 실제 데이터는 배열로 제공된다.
         * columns는 열이 어떻게 구성되어야 할 지에 대한 구조를 제공한다.
         */
        columns: any;
        /**
         * rows은 행을 의미하며 여러개의 ListValue를 가질수 있다.
         */
        rows: any[];
        constructor();
    }
    /**
     * 테이블 형태의 데이터
     */
    export class TableCard {
        /**
         * 테이블 type에는 데이터 형식에 대한 정보를 제공한다
         */
        type: string;
        /**
         * 테이블 Card의 아이템 목록들
         */
        items: TableItem[];
        constructor();
    }
    /**
     * 테이블 Card 내 ITEM
     */
    export class TableItem {
        /**
         * 키
         */
        key: string;
        /**
         * 값
         */
        value: string;
        /**
         * value에 대해서 적용되는 값 (예 : 정렬, 폰트, 컬러)
         */
        style: string;
        constructor();
    }
    /**
     * 리스트 형태의 테이블 형태의 데이터
     */
    export class TableListCard {
        /**
         * 테이블 형태의 카드를 배열로 처리한다.
         */
        cards: TableCard[];
        /**
         * 가로로 출력할지 여부..
         */
        horizontal: boolean;
        constructor();
    }
    /**
     * 선택형 카드
     *
     * 값을 선택할 수 있는 선택형 카드가 필요하다.
     */
    export class SelectCard {
        /**
         * 카드 전체의 타이블
         */
        title: string;
        /**
         * 카드의 헤데값
         */
        header: string;
        /**
         * 카드의 아이템 목록들
         */
        items: Item[];
        /**
         * 가로 본능?
         */
        horizontal: boolean;
        /**
         * 다양한 서브 타입을 넣을 수 있도록 한다.
         */
        type: string;
        constructor();
    }
    /**
     * 카드내 ITEM
     */
    export class Item {
        /**
         * 타이틀
         */
        title: string;
        /**
         * *optional* 요약
         */
        summary: string;
        /**
         * *opt* URL
         */
        imageUrl: string;
        /**
         * 선택될 경우에 전달된 텍스트
         * 이때에는 이벤트 메시지로 UtterType에 "TOUCH"를 전달하도록 한다.
         */
        selectedUtter: string;
        /**
         * Item이 여러개인 경우 대표 Item을 설정한다
         * selected 값이 true이면 해당 Item이 대표 Item 이다
         * Item이 두개이상인 경우 selected = true는 한개여야 한다
         */
        selected: boolean;
        /**
         * 예) "red large" 와 같이 특정 카드의 CSS를 직접 지정할 수 있도록 한다.
         */
        style: string;
        constructor();
    }
    /**
     * 링크 형태의 카드
     */
    export class LinkCard {
        /**
         * 제목
         */
        title: string;
        /**
         * 요약
         */
        summary: string;
        /**
         * 이미지 URL
         */
        imageUrl: string;
        /**
         * 이미지 클릭 후 이동할 URL
         */
        imageHref: string;
        /**
         * 다양한 서브 타입을 넣을 수 있도록 한다.
         */
        type: string;
        constructor();
    }
    /**
     * 리스트 형태의 카드들
     */
    export class ListCard {
        /**
         * 링크 형태의 카드를 배열로 처리한다.
         */
        cards: LinkCard[];
        /**
         * 가로로 출력할지 여부..
         */
        horizontal: boolean;
        constructor();
    }
    /**
     * 사용자 지정 카드
     */
    export class CustomCard {
        /**
         * 카드 타입
         */
        type: string;
        /**
         * 사용자 지정 카드는 모든 데이터를 다 가지고 있다.
         */
        cardData: Struct;
        constructor();
    }
    /**
     * 사용자 지정 카드 목록
     */
    export class CustomListCard {
        /**
         * 사용자 지정 카드의 목록
         */
        cards: CustomCard[];
        /**
         * 가로로 출력하도록 한다.
         */
        horizontal: boolean;
        constructor();
    }
    /**
     * 메시지 카드
     */
    export class Card {
        chart: ChartCard;
        table: TableCard;
        grid: GridCard;
        select: SelectCard;
        link: LinkCard;
        custom: CustomCard;
        linkList: ListCard;
        customList: CustomListCard;
        tableList: TableListCard;
        raw: string;
        constructor(init?: Partial<Card>);
        _chart: ChartCard;
        _table: TableCard;
        _grid: GridCard;
        _select: SelectCard;
        _link: LinkCard;
        _custom: CustomCard;
        _linkList: ListCard;
        _customList: CustomListCard;
        _tableList: TableListCard;
        _raw: string;
    }
    export enum AudioEncoding {
        /**
         * Not specified. Will return result [google.rpc.Code.INVALID_ARGUMENT][google.rpc.Code.INVALID_ARGUMENT].
         */
        ENCODING_UNSPECIFIED = "ENCODING_UNSPECIFIED",
        /**
         * Uncompressed 16-bit signed little-endian samples (Linear PCM).
         */
        LINEAR16 = "LINEAR16",
        /**
         * [`FLAC`](https://xiph.org/flac/documentation.html) (Free Lossless Audio
         * Codec) is the recommended encoding because it is
         * lossless--therefore recognition is not compromised--and
         * requires only about half the bandwidth of `LINEAR16`. `FLAC` stream
         * encoding supports 16-bit and 24-bit samples, however, not all fields in
         * `STREAMINFO` are supported.
         */
        FLAC = "FLAC",
        /**
         * 8-bit samples that compand 14-bit audio samples using G.711 PCMU/mu-law.
         */
        MULAW = "MULAW",
        /**
         * Adaptive Multi-Rate Narrowband codec. `sample_rate_hertz` must be 8000.
         */
        AMR = "AMR",
        /**
         * Adaptive Multi-Rate Wideband codec. `sample_rate_hertz` must be 16000.
         */
        AMR_WB = "AMR_WB",
        /**
         * Opus encoded audio frames in Ogg container
         * ([OggOpus](https://wiki.xiph.org/OggOpus)).
         * `sample_rate_hertz` must be 16000.
         */
        OGG_OPUS = "OGG_OPUS",
        /**
         * Although the use of lossy encodings is not recommended, if a very low
         * bitrate encoding is required, `OGG_OPUS` is highly preferred over
         * Speex encoding. The [Speex](https://speex.org/)  encoding supported by
         * Cloud Speech API has a header byte in each block, as in MIME type
         * `audio/x-speex-with-header-byte`.
         * It is a variant of the RTP Speex encoding defined in
         * [RFC 5574](https://tools.ietf.org/html/rfc5574).
         * The stream is a sequence of blocks, one block per RTP packet. Each block
         * starts with a byte containing the length of the block, in bytes, followed
         * by one or more frames of Speex data, padded to an integral number of
         * bytes (octets) as specified in RFC 5574. In other words, each RTP header
         * is replaced with a single byte containing the block length. Only Speex
         * wideband is supported. `sample_rate_hertz` must be 16000.
         */
        SPEEX_WITH_HEADER_BYTE = "SPEEX_WITH_HEADER_BYTE"
    }
    export class AuthenticationParam {
        /**
         * 인증 방법
         */
        method: string;
        /**
         * 값
         */
        value: string;
        constructor(method: string, value: string);
    }
    /**
     * 각 인증 방법에 따른 인증 파라미터들
     * 인증 페이로드
     */
    export class SignInPayload {
        /**
         * 사용자 키
         * 사용자를 구분할 수 있는 키
         */
        userkey: string;
        /**
         * 비밀번호
         */
        passphrase: string;
        authParams: AuthenticationParam[];
        device: Device;
        constructor(userkey: string, passphrase: string);
    }
    /**
     * 인증 결과 PAYLOAD
     */
    export class SignInResultPayload {
        authSuccess: AuthTokenPayload;
        authFailure: AuthFailurePayload;
        multiFactorAuthRequest: MultiFactorAuthRequestPayload;
        constructor();
    }
    /**
     * 인증 성공 데이터
     *
     * 인증 성공 시점에 단말에 전달되는 데이터
     * 단말은 인증 토콘을 저장해야 한다.
     */
    export class AuthTokenPayload {
        /**
         * 인증 요청에 대한 결과
         * 발급된 인증 토큰
         */
        authToken: string;
        /**
         * 이 인증 성공이 multi factor 인증의 결과물인가를 확인해준다.
         */
        multiFactor: boolean;
        /**
         * *optional*
         * 기타 인증 클라이언트에서 필요로 하는 추가적인 데이터
         */
        meta: Struct;
        constructor();
    }
    /**
     * 인증 실패 페이로드
     * 인증 싶패의 이유를 명시적으로 적시해준다.
     */
    export class AuthFailurePayload {
        /**
         * 인증 실패 코드
         */
        resCode: string;
        /**
         * 실패 메시지
         */
        message: string;
        /**
         * 인증 실패하는 경우에 대한 상세 메시지
         */
        detailMessage: string;
        constructor();
    }
    export class MultiFactorAuthMethod {
        /**
         * 추가적인 인증 요청 방식
         */
        method: string;
        /**
         * 추가적인 인증 요청을 위한 사용자 정의 데이터
         * 어떤 형식의 데이터도 처리할 수 있도록 Struct로 정의
         */
        param: Struct;
        constructor();
    }
    /**
     * MULTI FACTOR 인증을 위한 추가적인 인증 밥업과
     * 인증 방식을 정의합니다.
     */
    export class MultiFactorAuthRequestPayload {
        /**
         * 다음 `MultiFactorVerifyPayload` 을 보낼 때,
         * 함께 보내줘야 하는 ID
         */
        tempAuthToken: string;
        /**
         * 다중요소 인증 방법들의 배열
         * 서버에서 필요한 다양한 메시지가 정의될 수 있습니다.
         */
        multiFactorAuthMethods: MultiFactorAuthMethod[];
        constructor();
    }
    export class MultiFactorAuthResult {
        /**
         * 추가적인 인증 요청 방식
         */
        method: string;
        /**
         * 인증처리된 결과에 대한 값
         */
        value: string;
        /**
         * 해당 값 이외에 추가적으로 정의하려고 하는
         * 데이터 값,
         */
        meta: Struct;
        constructor();
    }
    /**
     * 다중 팩터 요청에 대한 상세 데이터
     */
    export class MultiFactorVerifyPayload {
        /**
         * 서버에서 보내온 다음 인증을 위한 키 데이터
         */
        tempAuthToken: string;
        /**
         * 각 인증 방법에 따른 인증 파라미터들
         */
        authParams: AuthenticationParam[];
        /**
         * 다중요소 인증 방법들의 배열
         * 서버에서 필요한 다양한 메시지가 정의될 수 있습니다.
         */
        multiFactorAuthResults: MultiFactorAuthResult[];
        device: Device;
        constructor();
    }
    /**
     * SignOut 페이로드
     */
    export class SignOutPayload {
        /**
         * 인증 토큰
         */
        authToken: string;
        /**
         * 사용자 키 (optional)
         */
        userKey: string;
        constructor(authtoken: string);
    }
    /**
     * 응답 메시지
     */
    export class SignOutResultPayload {
        /**
         * 로그아웃 후 응답 메시지
         */
        message: string;
        constructor();
    }
    /**
     * 사용자 속성 정의 데이터
     */
    export class UserSettings {
        /**
         * 인증 토큰
         */
        authToken: string;
        /**
         * 사용자 속성
         */
        settings: Struct;
        constructor();
    }
    /**
     * 사용자 속성 조회를 위한 키
     */
    export class UserKey {
        /**
         * 인증 토큰
         */
        authToken: string;
        constructor();
    }
    /**
     * 주기적인 Ping 요청
     */
    export class PingRequest {
        /**
         * 디바이스 정보
         */
        device: Device;
        /**
         * PING 호출 시간
         */
        pingAt: Date;
    }
    /**
     * PONG을 통한 클라이언트 상태 표시
     */
    export enum PongClientState {
        /**
         * 기존에 이미 존재한 클라이언트 지속
         */
        PONG_CLIENT_CONTINUE = "PONG_CLIENT_CONTINUE",
        /**
         * 새로운 클라이언트가 발견되었고 이를 등록처리
         */
        PONG_NEW_CLIENT_FOUND = "PONG_NEW_CLIENT_FOUND",
        /**
         * 기존의 클라이언트가 재시작 되었음을 인지했음.
         */
        PONG_IDLE_CLIENT_RESTART = "PONG_IDLE_CLIENT_RESTART",
        /**
         * 접근이 거부되었음
         */
        PONG_ACCESS_DENIED = "PONG_ACCESS_DENIED"
    }
    /**
     * PONG 응답
     */
    export class PongResponse {
        /**
         * 클라이언트 상태
         */
        clientState: PongClientState;
        /**
         * 서버 식별 정보
         */
        m2uId: MaumToYouIdentifier;
        /**
         * 새로운 EventStream을 요청할 필요가 있는지 여부
         */
        requireEventStream: boolean;
        dirState: DirectiveState;
        version: string;
        pongAt: Date;
        constructor();
    }
    /**
     * M2U 서버에 대한 구분
     */
    export class MaumToYouIdentifier {
        uuid: string;
        version: string;
        licensedTo: string;
        constructor();
    }
    /**
     * 디렉티브 상태
     */
    export class DirectiveState {
        pending: boolean;
        pendingCount: number;
        discarded: boolean;
        discardedCount: number;
        constructor();
    }
    export class EventContext {
        /**
         * 디바이스 정보
         */
        private device;
        /**
         * 위치 정보
         */
        private location;
        constructor();
        setDevice(device: Device): void;
        setLocation(loc: Location): void;
    }
    export class EventParam {
        private speechRecognitionParam;
        private imageRecognitionParam;
        /**
         * 비디오 파라미터
         */
        private videoParam;
        /**
         * 이미지 파라미터
         */
        private gestureParam;
        /**
         * 키보드 파라미터
         */
        private keyboardParam;
        /**
         * 빈 파라미터
         */
        private emptyParam;
        /**
         * DA에 호출 정보를 넘겨준다.
         */
        private dialogAgentForwarderParam;
        constructor();
        _speechRecognitionParam: SpeechRecognitionParam;
        _imageRecognitionParam: ImageRecognitionParam;
        _videoParam: VideoParam;
        _gestureParam: GestureParam;
        _keyboardParam: KeyboardParam;
        _emptyParam: Empty;
        _dialogAgentForwarderParam: DialogAgentForwarderParam;
    }
    /**
     * 이벤트 형식의 FRAME
     * 단일 이벤트이거나 연속된 스트림 이벤트의 시작일 수 있다.
     */
    export class EventStream {
        /**
         * 호출되는 대상에 대한 정의
         * 호출되는 대상은 interface와 operation으로 정의된다.
         * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
         */
        interface: AsyncInterface;
        /**
         * 호출 ID
         * AsyncInterface 메시지의 ID에서 유일하게 생성된다.
         */
        streamId: string;
        /**
         * 요청 동기화 ID *Optional*
         * 이벤트가 대응이 되는 응답이 있을 경우에 이를 정의할 수 있다.
         * 예를 들어서 AudioTalk의 이벤트를 클라이언트에서 전송할 경우에는
         * 응답으로 음성인식이 완료됨, 응답 메시지의 전송, 응답 화면 출력 등 디렉티브를
         * 받을 수 있다.
         */
        operationSyncId: string;
        /**
         * 이벤트 컨텍스트
         * 컨텍스트는 한 개 이상 전송될 수 있다.
         */
        contexts: EventContext[];
        /**
         * Event에는 BODY Stream에 대한 파라미터가 존재할 수 있다.
         * 각 파라미터는 스트림 BODY 규격에 따라서 다르다. 자세한 사항은
         * 각 AsyncInterface에 규격표에 정리하여 정의할 수 있다.
         * 이벤트 파라미터는 한개 존재한다.
         */
        param: EventParam;
        /**
         * 이벤트에 대한 메타데이터는 추가적인 메시지로서 각 AsyncInterface 또는 사용자의 정의에
         * 따라서 임의의 메시지가 전송될 수 있다.
         */
        payload: Struct;
        /**
         * 이벤트가 시작된 시간
         */
        beginAt: Date;
        constructor();
    }
    export class DirectiveParam {
        /**
         * DA에서 단말에게 전달되는 param
         */
        daForwardParam: DialogAgentForwarderParam;
        /**
         * TTS 음성인식 파라미터
         */
        private speechSynthesizerParam;
        /**
         * 비디오 파라미터
         */
        private videoParam;
        /**
         * 빈 파라미터
         */
        private emptyParam;
        constructor();
    }
    /**
     * 디렉티브 형식의 프레임
     * 단일한 디렉티브 이거나 연속된 스트림 디렉티브의 시작일 수 있다.
     */
    export class DirectiveStream {
        /**
         * 호출되는 대상에 대한 정의
         * 호출되는 대상은 interface와 operation으로 정의된다.
         * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
         */
        interface: AsyncInterface;
        /**
         * 호출 메시지를 구분하는 ID
         */
        streamId: string;
        /**
         * 대화와 관련된 응답 메시지 규격 *Optional*
         */
        operationSyncId: string;
        param: DirectiveParam;
        /**
         * 디렉티브에 대한 메타데이터는 추가적인 메시지로서 각 AsyncInterface
         * 또는 사용자의 정의에 따라서 임의의 메시지가 전송될 수 있다.
         */
        payload: Struct;
        /**
         * 디렉티브가 시작된 시간
         */
        beginAt: Date;
        constructor();
    }
    export enum StatusCode {
        STATUS_NOT_SPECIFIED = "STATUS_NOT_SPECIFIED",
        GRPC_STT_ERROR = "GRPC_STT_ERROR",
        GRPC_IDR_ERROR = "GRPC_IDR_ERROR",
        GRPC_TTS_ERROR = "GRPC_TTS_ERROR",
        GRPC_AUTH_SIGN_IN_ERROR = "GRPC_AUTH_SIGN_IN_ERROR",
        GRPC_AUTH_SIGN_OUT_ERROR = "GRPC_AUTH_SIGN_OUT_ERROR",
        GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR = "GRPC_AUTH_MULTIFACTOR_VERIFY_ERROR",
        GRPC_AUTH_IS_VALID_ERROR = "GRPC_AUTH_IS_VALID_ERROR",
        GRPC_AUTH_GET_USER_INFO_ERROR = "GRPC_AUTH_GET_USER_INFO_ERROR",
        GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR = "GRPC_AUTH_UPDATE_USER_SETTINGS_ERROR",
        GRPC_AUTH_GET_USER_SETTINGS_ERROR = "GRPC_AUTH_GET_USER_SETTINGS_ERROR",
        GRPC_ROUTER_OPEN_ERROR = "GRPC_ROUTER_OPEN_ERROR",
        GRPC_ROUTER_TALK_ERROR = "GRPC_ROUTER_TALK_ERROR",
        GRPC_ROUTER_EVENT_ERROR = "GRPC_ROUTER_EVENT_ERROR",
        GRPC_ROUTER_CLOSE_ERROR = "GRPC_ROUTER_CLOSE_ERROR",
        GRPC_ROUTER_FEEDBACK_ERROR = "GRPC_ROUTER_FEEDBACK_ERROR",
        GRPC_STT_TRANSCRIPT_NULL_ERROR = "GRPC_STT_TRANSCRIPT_NULL_ERROR",
        AUTH_IS_VAILD_FAILED = "AUTH_IS_VAILD_FAILED",
        AUTH_INVALID_AUTH_TOKEN = "AUTH_INVALID_AUTH_TOKEN",
        AUTH_FAILED = "AUTH_FAILED",
        AUTH_INVALID_HEADER = "AUTH_INVALID_HEADER",
        AUTH_CHECK_AUTH_FAILED = "AUTH_CHECK_AUTH_FAILED",
        MAP_NO_STREAM_PARAM = "MAP_NO_STREAM_PARAM",
        MAP_IF_NOT_FOUND = "MAP_IF_NOT_FOUND",
        MAP_IF_STREAMING_NOT_MATCH = "MAP_IF_STREAMING_NOT_MATCH",
        MAP_IF_DUPLICATED_STREAMING = "MAP_IF_DUPLICATED_STREAMING",
        MAP_EVENT_CASE_NOT_SET = "MAP_EVENT_CASE_NOT_SET",
        MAP_CURRENTLY_HAS_NO_STREAM = "MAP_CURRENTLY_HAS_NO_STREAM",
        MAP_STREAM_ID_NOT_MATCH = "MAP_STREAM_ID_NOT_MATCH",
        MAP_PAYLOAD_ERROR = "MAP_PAYLOAD_ERROR",
        MAP_CLASS_NOT_FOUND = "MAP_CLASS_NOT_FOUND",
        ROUTER_SESSION_NOT_FOUND = "ROUTER_SESSION_NOT_FOUND",
        ROUTER_SESSION_INVALID = "ROUTER_SESSION_INVALID",
        ROUTER_CHATBOT_NOT_FOUND = "ROUTER_CHATBOT_NOT_FOUND",
        ROUTER_DA_NOT_FOUND = "ROUTER_DA_NOT_FOUND",
        ROUTER_DA_ERROR = "ROUTER_DA_ERROR",
        ROUTER_ITF_NOT_FOUND = "ROUTER_ITF_NOT_FOUND",
        ROUTER_ITF_ERROR = "ROUTER_ITF_ERROR",
        MAP_TOTAL_SESSION_COUNT_EXCEEDED = "MAP_TOTAL_SESSION_COUNT_EXCEEDED",
        MAP_SYSTEM_MAINTENANCE = "MAP_SYSTEM_MAINTENANCE"
    }
    /**
     * 예외는 Event나 Directive에 대응하여 호출시 문제가 생기면 이를 내보내도록 한다.
     */
    export class MapException {
        /**
         * 예외를 위한 ID, stream_id와 동일한 레벨임.
         */
        exceptionId: string;
        /**
         * 상태 정보
         */
        statusCode: StatusCode;
        /**
         * 상세한 서버의 예외 메시지 정보
         */
        exMessage: string;
        /**
         * 상세한 서버의 예외 메시지 정보
         */
        exIndex: number;
        /**
         * 상세한 예외 본문
         */
        payload: Struct;
        /**
         * 예외 전송 시각
         */
        thrownAt: Date;
        /**
         * *Optional* 예외가 발생한 ID
         * 예외가 인증에러와 관련된 것들이 아닌 경우에는
         */
        streamId: string;
        /**
         * *Optional* 예외와 관련된 operation_sync_id
         */
        operationSyncId: string;
        /**
         * *Optional* 예외와 관련된 AsyncInterface
         */
        calledInterface: AsyncInterface;
        constructor();
    }
    /**
     * `StreamMeta`을 통해서
     * Event나 Directive에 추가적인 스트림 데이터를 전송할 수 있다.
     * 기본적인 스트림 데이터 형식인 문자열이나 바이트 배열 이외에 다른 메시지를
     * 포함하려고 할 때 사용할 수 있다.
     */
    export class StreamMeta {
        /**
         * 객체 타입, 메타 데이터의 객체 유형을 정의 한다.
         */
        objectType: string;
        /**
         * 메타 데이터, 메타 데이터를 정의한다.
         */
        meta: Struct;
        constructor();
    }
    /**
     * 스트림 형식의 Event나 Directive의 끝을 표시해준다.
     */
    export class StreamEnd {
        /**
         * 종료시키려는 FRAME을 명시적으로 정의한다.
         */
        streamId: string;
        /**
         * 종료 시간
         */
        endAt: Date;
        constructor();
    }
    /**
     * 중지를 발생시키는 타입
     */
    export enum StreamBreaker {
        /**
         * 원인을 알 수 없는 중지
         */
        BREAK_BY_UNSPECIFIED = "BREAK_BY_UNSPECIFIED",
        /**
         * 사용자에 의한 명시적인 중지
         */
        BREAK_BY_USER = "BREAK_BY_USER",
        /**
         * 클라이언트나 서버의 내부 구조에 의한 중지
         */
        BREAK_BY_SYSTEM = "BREAK_BY_SYSTEM",
        /**
         * Stream 전송 규칙 과정에서 발생하는 논리적인 오류
         */
        BREAK_BY_LOGICAL = "BREAK_BY_LOGICAL",
        /**
         * 예외에 의한 중지
         * 예를 들어서 음성 데이터를 받아오는 네트워크의 중지에 의한 예외 등
         */
        BREAK_BY_EXCEPTION = "BREAK_BY_EXCEPTION"
    }
    export class StreamBreak {
        constructor();
        /**
         * 중지 이유 메시지, 빈 문자열일 수 있수 없다. 중지 사유를
         * 로그로 남겨야 하므로 이를 기록해야 한다.
         * 로그를 남길 수 없는 경우에 프로그램은 경고 메시지를 남겨야 한다.
         */
        reason: string;
        /**
         * 멈춤 메시지 구분자
         */
        breaker: StreamBreaker;
        /**
         * *Optional* 중지시키는 메시지 ID
         * 중지시키는 메시지 ID는 참고용으로만 사용되므로 유의미하지는
         * 않다.
         * 메시지 BREAK가 발생한 이후로 들어오는 모든 bytes, text는
         * 무시된다.
         */
        streamId: string;
        /**
         * 중지 시점
         */
        brokenAt: Date;
    }
    /**
     * operation 타입
     */
    export enum OperationType {
        /**
         * EVENT 유형, `클라이언트`에서 `MAP`으로 전송된다.
         */
        OP_EVENT = "OP_EVENT",
        /**
         * DIRECTIVE 유형, `MAP`에서 `클라이언트`로 전송된다.
         */
        OP_DIRECTIVE = "OP_DIRECTIVE"
    }
    /** 호출되는 대상에 대한 정의
     * 호출되는 대상은 interface와 operation으로 정의된다.
     * operation은 단순히 메소드가 아니라 이벤트 메시지이기도 하고
     * 각 상대방이 처리해야할 메시지이기도 하다.
     *
     * 예를 들어, interface는 VideoPlayer 이고 operation 은 stop 이다.
     */
    export class AsyncInterface {
        /**
         * 인터페이스 이름
         * 통상적으로 변수 규칙에 따르고 대문자로 시작하고 Camel Case로 작성한다.
         * Map과 통신하는 구조에서는 기본으로 제공하는 인터페이스 외에 추가적인 인터페이스가
         * 존재할 수 있다.
         * 추가적인 인터페이스에 대해서는 플러그인 형태로 개발할 수 있도록 되어 있다.
         */
        interface: string;
        /**
         * 오퍼레이션 이름
         * 각 상대방에서 수행해야 할 작업의 이름이고 인터페이스에 종속적이다.
         * 이 이름은 동사이거나 발생하는 이벤트의 이름이어야 한다.
         */
        operation: string;
        /**
         * 메시지의 타입은 EVENT 이거나 DIRECTIVE 임.
         */
        type: OperationType;
        /**
         * 스트리밍 여부
         * operation은 스트리밍 형식이거나 단일 메시지 형식이다.
         * 스트리밍 형식의 operation은 EventStream, DirectiveStream과 StreamEnd 사이에
         * 반복적인 BODY 메시지를 전송할 수 있다.
         * 반복적인 BODY 형식은 string, byte array, meta 형식으로 전송될 수 있다.
         * EventStream, DirectiveStream과 StreamEnd은 같은 stream_id를 공유한다.
         * EventEnd를 만나면 해당 스트림은 종료된다.
         * 스트리밍이 아닌 AsyncInterface 형식을 가진 EventStream이나 DirectiveStream이
         * 중간에 끼여서 전송될 수 있다. 하지만, 스트리밍 이벤트 중간에 다른 스트리밍 이벤트는
         * 전송될 수 없다. 만일, 동시에 스트리밍 이벤트는 여러 개 전송해야 하는 경우에는
         * 새로운 grpc 연결을 사용해서 처리하면 된다.
         * 이 규칙을 어기고 스트림 형식의 새로운 EventStream이나 DirectiveStream이 들어오면
         * 현재의 스트림은 자동으로 종료 처리된다.
         * 스트리밍 프레임들은 StreamBreak 메시지를 통해서 중지될 수 있다.
         * 이 경우, 스트리밍 프레임은 중지되고, stream_id는 더 이상 유효하지 않게 된다.
         * 추가적인 스트리밍 BODY는 모두 버려지게 된다.
         */
        streaming: boolean;
        constructor(init?: Partial<AsyncInterface>);
    }
    /**
     * 비동기인터페이스의 목록
     */
    export class AsyncInterfaceList {
        interfaces: AsyncInterface[];
        constructor();
    }
    /**
     * Video 관련 AsyncInterface를 호출할 때 사용될 수 있는 파라미터.
     */
    export class VideoParam {
        videoFormat: string;
        codec: string;
        todo: string;
        constructor();
    }
    export class GestureParam {
        todo: string;
        constructor();
    }
    /**
     * 키보드 파라미터
     */
    export class KeyboardParam {
        /**
         * 입력 길이
         */
        typedLength: number;
        /**
         * 입력 시간
         */
        duration: Duration;
        constructor();
    }
    /**
     * RenderText 호출에 따르는 Payload
     */
    export class RenderTextPayload {
        text: string;
        constructor();
    }
    /**
     * 기존 V1, V2 호환을 위해서 또는 디버그 목적으로 DA에서 내려주는 다양한 정보들을
     * 사용하도록 내려준다.
     * 이때 meta가 그대로 내려간다.
     */
    export class RenderHiddenPayload {
        /**
         * 기존 V1, V2 호환을 위해서 또는 디버그 목적으로 DA에서 내려주는 다양한 정보들을
         * 사용하도록 내려준다.
         * 이때 meta가 그대로 내려간다.
         */
        meta: Struct;
        constructor();
    }
    /**
     * 음성입력 대기에 필요한 추가적인 Payload 데이터
     */
    export class ExpectSpeechPayload {
        /**
         * 입력 대기 시간, milli second
         */
        timeoutInMilliseconds: number;
        constructor();
    }
}
declare module "native" {
    import { AsyncInterface, Device, DirectiveStream, EventStream, Location, MapException, PongResponse, SpeechRecognitionParam } from "proto";
    /**
     * MapSetting은 초기 데이터로 NATIVE INTERFACE를 생성하는 역할을 한다.
     * 이 설정은 주기적으로 새롭게 정의할 수 있다.
     */
    export class MapSetting {
        /**
         * 서버 주소
         */
        serverIp: string;
        /**
         * 서버 포트
         */
        serverPort: number;
        /**
         * TLS 사용 여부
         */
        useTls: boolean;
        /**
         * notify를 호출할 때 사용할 prefix
         * 만일, prefix가 "webviewMapClient" 라면
         * "webviewMapClient.receiveDirective( + string  + )"
         * 형식으로 호출해주시면 됩니다.
         */
        notifyObjectPrefix: string;
        /**
         * 주기적인 PING 전송할지 여부
         */
        pingInterval: number;
        /**
         * 인증 토큰
         */
        authToken: string;
        /**
         * 기본 단말 정보
         * 단말 정보가 갱신되면 계속해서 MapSetting을 통해서
         * 추가적인 정보를 저장하도록 한다.
         * location은 제외..
         */
        device: Device;
        /**
         * 생성자
         * @param {string} serverIp 서버 주소
         * @param {number} serverPort 서버 포트
         * @param {boolean} useTls TLS 사용 여부
         */
        constructor(serverIp: string, serverPort: number, useTls: boolean);
        /**
         * 인증 토콘을 설정한다.
         * @param {string} token
         */
        setAuthToken(token: string): void;
        /**
         * 핑 주기를 설정한다.
         * @param {number} interval
         */
        setPingInterval(interval: number): void;
    }
    /**
     * MAP과 통신할 때 발생할 수 있는 에러 메시지
     */
    export class GrpcStatus {
        /**
         * 에러코드
         */
        errorCode: number;
        /**
         * 에러 메시지
         */
        errorMessage: string;
        /**
         * 에러 상세 정보
         */
        errorDetail: string;
        /**
         * 바이너리를 복원하면, object, json
         *
         */
        errorObject: Object;
        constructor();
    }
    /**
     * 스트리밍 유형
     *
     * map에 스트리밍 형태로 데이터를 전송할 때
     * 계속되는 데이터의 유형, 크게 3가지가 있다.
     */
    export enum StreamingType {
        /**
         * 타임 없음
         */
        UNKONWN = "UNKONWN",
        /**
         * 바이트 형태
         */
        BYTES = "BYTES",
        /**
         * 텍스트 형태
         */
        TEXT = "TEXT",
        /**
         * 사용자 정의 데이터 형태
         */
        STRUCT = "STRUCT"
    }
    /**
     * 스트리밍 전송 상태를 알려주는 데이터 형식
     */
    export class StreamingStatus {
        /**
         * 스트림 ID
         */
        streamId: string;
        /**
         * 응답 동기화 ID
         */
        operationSyncId: string;
        /**
         * 이벤트인가 디렉티브인가
         */
        isEvent: boolean;
        /**
         * 이벤트가 강제로 중지되었는가?
         */
        isBreak: boolean;
        /**
         * 이벤트의 끝인가?
         */
        isEnd: boolean;
        /**
         * 스트리밍 데이터의 구체적인 형식
         */
        type: StreamingType;
        /**
         * 스트리밍의 크기
         */
        size: number;
        /**
         * 호출한 인터페이스의 이름
         */
        interface: AsyncInterface;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 마이크 개방 파라미터
     */
    export class MicrophoneParam {
        /**
         * 아이크 대기 모드인가?
         */
        expectMode: boolean;
        /**
         * Microphone ON 유지 시간 (milli sec)
         * 밀리시간 단위로 내보내는 대기 시간
         * recognizing 이면 timer restart
         * default: 10,000
         */
        timoeoutInMilliseconds: number;
        /**
         * STT 인식을 위한 파라미터
         */
        speechParam: SpeechRecognitionParam;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 마이크 상태 이벤트
     */
    export enum MicrophoneEvent {
        /**
         * UNKNOWN
         */
        UNKNOWN = "UNKNOWN",
        /**
         * 마이크 열기 성공
         */
        OPEN_SUCCESS = "OPEN_SUCCESS",
        /**
         * 마이크 열기 실패
         */
        OPEN_FAILURE = "OPEN_FAILURE",
        /**
         * 마이크 데이터 캡춰 시작
         */
        STARTING_RECORD = "STARTING_RECORD",
        /**
         * 닫기 성공
         */
        CLOSE_SUCCESS = "CLOSE_SUCCESS",
        /**
         * 닫기 실패
         */
        CLOSE_FAILURE = "CLOSE_FAILURE"
    }
    /**
     * 마이크 실패에 대한 상세 유형
     */
    export enum MicrophoneFailure {
        /**
         * 열기 실패 알수 없는 실패 또는 성공
         */
        MICROPHONE_ERROR_UNKNOWN = "MICROPHONE_ERROR_UNKNOWN",
        /**
         * 열기 에러
         */
        MICROPHONE_OPEN_ERROR = "MICROPHONE_OPEN_ERROR",
        /**
         * 닫기 에러
         */
        MICROPHONE_CLOSE_ERROR = "MICROPHONE_CLOSE_ERROR",
        /**
         * 사용 중
         */
        MICROPHONE_UNDER_USE = "MICROPHONE_UNDER_USE",
        /**
         * 대기 타이머 초과
         */
        MICROPHONE_TIMER_EXPIRED = "MICROPHONE_TIMER_EXPIRED",
        /**
         * 장치 접근이 거부됨
         */
        MICROPHONE_PERMISSION_DENIED = "MICROPHONE_PERMISSION_DENIED"
    }
    /**
     * 마이크 상태
     */
    export class MicrophoneStatus {
        /**
         * 이벤트 유형
         */
        eventType: MicrophoneEvent;
        /**
         * 현재가 export mode 인가?
         */
        expectMode: boolean;
        /**
         * 추가적인 메시지 여부
         */
        hasReason: boolean;
        /**
         * 에러 유형
         */
        failure: MicrophoneFailure;
        /**
         * 상세한 에러 메시지
         */
        message: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 스피커 이벤트
     */
    export enum SpeakerEvent {
        /**
         * 스피커 데이터 출력 중
         */
        MUTE = "MUTE",
        /**
         * 스피커 데이터 없음
         */
        UNMUTE = "UNMUTE",
        /**
         * 스피커 닫힘
         */
        CLOSED = "CLOSED"
    }
    /**
     * 스피커 에러 유형
     */
    export enum SpeakerFailure {
        /**
         * 알수 없는 에러 또는 성공
         */
        UNKONWN = "UNKONWN",
        /**
         * 닫기 실패
         */
        SPAEKER_CLOSE_FAILED = "SPAEKER_CLOSE_FAILED",
        /**
         * 장치 접근이 거부됨
         */
        SPAEKER_PERMISSION_DENIED = "SPAEKER_PERMISSION_DENIED"
    }
    /**
     * 스피커 상태
     */
    export class SpeakerStatus {
        /**
         * 스피커 이벤트
         */
        eventType: SpeakerEvent;
        /**
         * 현재 음성 출력 중인가?
         * 노래 출력 중일 경우에는 FALSE입니다.
         */
        speechSynthesizing: boolean;
        /**
         * 추가적인 메시지 여부
         */
        hasReason: boolean;
        /**
         * 스피키 여러 상세 유형
         */
        failure: SpeakerFailure;
        /**
         * 상세한 에러 메시지
         */
        message: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 카메라 해상도
     */
    export class Resolution {
        /**
         * 최소 너비
         */
        min_x: number;
        /**
         * 최소 높이
         */
        min_y: number;
        /**
         * 최대 너비
         */
        max_x: number;
        /**
         * 최대 높이
         */
        max_y: number;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 카메라 파라미터
     */
    export class CameraParam {
        /**
         * rear Camera = true, front Camera: false
         */
        rearCamera: boolean;
        /**
         * 확인결과 preview 상태일 때 App이 Background이므로 보통의 경우
         * Timer 를 구동시켜서 닫는 방법을 사용합니다.
         * 따라서 CameraDTO 객체에 timer interval을 추가하였습니다.
         *
         */
        openInterval: number;
        /**
         * 해상도
         */
        res: Resolution;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 카메라 이벤트
     */
    export enum CameraEvent {
        /**
         * 알수 없는 이벤트
         */
        UNKNOWN = "UNKNOWN",
        /**
         * 열기 성공
         */
        OPEN_SUCCESS = "OPEN_SUCCESS",
        /**
         * 열기 닫기
         */
        OPEN_FAILURE = "OPEN_FAILURE",
        /**
         * 이미지 캡춰됨
         */
        IMAGE_CAPTURED = "IMAGE_CAPTURED",
        /**
         * 카메라 닫힘
         */
        CLOSED = "CLOSED"
    }
    /**
     * 카메라 실패 유형
     */
    export enum CameraFailure {
        /**
         * 알수 없는 에러 또는 성공
         */
        CAMERA_ERROR_UNKNOWN = "CAMERA_ERROR_UNKNOWN",
        /**
         * 열기 실패
         */
        CAMERA_OPEN_ERROR = "CAMERA_OPEN_ERROR",
        /**
         * 닫기 실패
         */
        CAMERA_CLOSE_ERROR = "CAMERA_CLOSE_ERROR",
        /**
         * 캡춰 취소
         * 카메라에서 명시적으로 사용자에 의한 취소
         */
        CAMERA_CAPTURE_CANCELLED = "CAMERA_CAPTURE_CANCELLED",
        /**
         * 메모리 부족
         */
        CAMERA_INSUFFICIENT_MEMORY = "CAMERA_INSUFFICIENT_MEMORY",
        /**
         * 저장 실패
         */
        CAMERA_STORE_FAILED = "CAMERA_STORE_FAILED",
        /**
         * 카메라 타이머 지남
         */
        CAMERA_TIMER_EXPIRED = "CAMERA_TIMER_EXPIRED",
        /**
        * 장치 접근이 거부됨
        */
        CAMERA_PERMISSION_DENIED = "CAMERA_PERMISSION_DENIED"
    }
    /**
     * 이미지 데이터
     */
    export class ImageData {
        /**
         * 로컬 장치 내부에 유지되는 이미지를 구분하는 리소스 경로
         */
        imageUrl: string;
        /**
         * capture 된 image의 Thumbnail data(base64 Encoded)
         */
        thumbnail: string;
        /**
         * 캡춰된 해상도
         */
        res: Resolution;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 카머라 상태 정보
     */
    export class CameraStatus {
        /**
         * 카메라 이벤트
         */
        eventType: CameraEvent;
        /**
         * 이미지 데이터
         */
        imageData: ImageData;
        /**
         * 추가적인 정보 여부
         */
        hasReason: boolean;
        /**
         * 카메라 에러 유형
         */
        failure: CameraFailure;
        /**
         * 상세한 에러 메시지
         */
        message: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 갤러리 열기 파라미터
     */
    export class GalleryParam {
        /**
         * 열기 대기 시간
         *
         * Gallery Open Interval(10의 경우 10초 동안 갤러리 이미지 선택까지 하지 않는 경우
         * 자동 종료되고 close/GALLERY_TIMER_EXPIRED가 JS에 전송됨)
         */
        openInterval: number;
        /**
         * 원하는 해상도 크기
         */
        res: Resolution;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 갤러리에서 발생하는 이벤트
     */
    export enum GalleryEvent {
        /**
         * 알수 없는 이벤트
         */
        UNKNOWN = "UNKNOWN",
        /**
         * 열기 성공
         */
        OPEN_SUCCESS = "OPEN_SUCCESS",
        /**
         * 열기 실패
         */
        OPEN_FAILURE = "OPEN_FAILURE",
        /**
         * 이미지 선택 완료
         */
        IMAGE_CAPTURED = "IMAGE_CAPTURED",
        /**
         * 갤러리 닫힘
         */
        CLOSED = "CLOSED"
    }
    /**
     * 갤러리 실패 유형
     */
    export enum GalleryFailure {
        /**
         * 갤러리 모르는 에러 및 성공
         */
        GALLERY_ERROR_UNKNOWN = "GALLERY_ERROR_UNKNOWN",
        /**
         * 열기 실패
         */
        GALLERY_OPEN_ERROR = "GALLERY_OPEN_ERROR",
        /**
         * 닫기 실패
         */
        GALLERY_CLOSE_ERROR = "GALLERY_CLOSE_ERROR",
        /**
         * 사용자에 의한 선태 취소
         */
        GALLERY_SELECT_CANCELLED = "GALLERY_SELECT_CANCELLED",
        /**
         * 시간이 지나서 자동으로 닫힘
         */
        GALLERY_TIMER_EXPIRED = "GALLERY_TIMER_EXPIRED",
        /**
         * 장치 접근이 거부됨
         */
        GALLERY_PERMISSION_DENIED = "GALLERY_TIMER_EXPIRED"
    }
    /**
     * 갤러리 상태 정보
     */
    export class GalleryStatus {
        /**
         * 갤러리 이벤트 유형
         */
        eventType: GalleryEvent;
        /**
         * 선택된 이미지 데이터
         */
        imageData: ImageData;
        /**
         * 추가적인 메시지 여부
         */
        hasReason: boolean;
        /**
         * 갤러리 에러의 상세한 정보
         */
        failure: GalleryFailure;
        /**
         * 추가적인 메시지
         */
        message: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 위치 정보 획득 실패 이유
     */
    export enum LocationFailure {
        LOCATION_UNKONWN = "LOCATION_UNKONWN",
        /**
         * 장치 접근이 거부됨
         */
        LOCATION_PERMISSION_DENIED = "LOCATION_PERMISSION_DENIED"
    }
    /**
     * 위치 정보 획득 상태
     *
     */
    export class LocationStatus {
        /**
         * 구해 놓은 위치
         */
        location: Location;
        /**
         * 실패 이유가 있음
         */
        hasReason: boolean;
        /**
         * 실패 이유
         */
        failure: LocationFailure;
        /**
         * 상세한 메시지
         */
        message: string;
        /**
         * 생성자
         */
        constructor();
    }
    /**
     * 웹뷰가 구현하고 있는 코크들에 대한 정의
     *
     * 웹뷰에서 네이티브로 호출하는 함수들에 대한 규격을 정의한다.
     */
    export interface M2uWebViewClientNative {
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {MapSetting} setting
         * @returns {boolean}
         * @desc Js to Native
         */
        setMapSetting(setting: MapSetting): boolean;
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {EventStream} es 서버로 보낼 이벤트 메시지
         * @param {string | null} opt 이미지 URL
         * @desc Js to Native
         */
        sendEvent(es: EventStream, opt: string | null): void;
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {MicrophoneParam} param 개방 옵션
         * @desc JS to Native
         */
        openMicrophone(param: MicrophoneParam): void;
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        closeMicrophone(): void;
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        closeSpeaker(): void;
        /**
         * 카메라를 엽니다.
         *
         * @param {CameraParam} param
         * @desc JS To Native
         */
        openCamera(param: CameraParam): void;
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        closeCamera(): void;
        /**
         * 갤러리를 오픈한다.
         *
         * @param {GalleryParam} param
         */
        openGallery(param: GalleryParam): void;
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        closeGallery(): void;
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        getLocation(): void;
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        getPhoneNumber(): any;
    }
    /**
     * 웹뷰가 구현하고 있는 코크들에 대한 정의
     *
     * 웹뷰에서 네이티브로 호출하는 함수들에 대한 규격을 정의한다.
     */
    export interface M2uWebViewClientNativeMobile {
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {string} setting, MapSetting 값의 json string 버전
         * @returns {boolean}
         * @desc Js to Native
         */
        setMapSetting(setting: string): boolean;
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
         * @param {string} imageUrl 이미지 URL
         * @desc Js to Native
         */
        sendEvent(es: string, imageUrl?: string): void;
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
         * @desc JS to Native
         */
        openMicrophone(param: string): void;
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        closeMicrophone(): void;
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        closeSpeaker(): void;
        /**
         * 카메라를 엽니다.
         *
         * @param {string} param 문자열로 변환된 CameraParam 객체
         * @desc JS To Native
         */
        openCamera(param: string): void;
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        closeCamera(): void;
        /**
         * 갤러리를 오픈한다.
         *
         * @param {GalleryParam} param
         */
        openGallery(param: string): void;
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        closeGallery(): void;
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         * @desc JS to Native
         */
        getLocation(): void;
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         * @desc JS to Native
         */
        getPhoneNumber(): any;
    }
    /**
     * 네이티브에서 JAVASCRIPT를 호출할 때 사용하는 정보
     *
     * 아래와 같은 인터페이스 구조로 동작합니다.
     */
    export interface M2uWebViewClientNativeEventForward {
        /**
         * MAP 서버로부터 응답을 받은다.
         * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
         *
         * @param {string} dir 응답받은 메시지 데이터
         * @desc Native to JS
         */
        receiveDirective(dir: string): void;
        /**
         * MAP directive 수신 완료
         * @param {string} end  operation sync id를 문자욜로 보낸다.
         * JSON으로 포장하지 않고 syncid만 그대로 전송한다.
         */
        receiveCompleted(end: string): void;
        /**
         * map 통신 중에 수신한 exception
         *
         * mapdirective의 내용을 json 형태로 전송한다.
         *
         * @param {string} ex MapException MAP 통신 중에서 Exception이 발생한 경우
         * @desc Native to JS
         */
        receiveException(ex: string): void;
        /**
         * 에러를 수신한다.
         * grpc Error를 수신한 경우에 이를 담아서 보내준다
         *
         * @param {string} status GrpcStatus GRPC 에러 생태 값 및 상세 정보
         * @desc Native to JS
         */
        receiveError(status: string): void;
        /**
         * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
         * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus 타입
         * @desc Native To JS
         */
        sendStreamingEvent(st: string): void;
        /**
         * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
         * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
         * 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus
         * @desc Native To JS
         */
        receiveStreamingDirective(st: string): void;
        /**
         * Ping의 결과가 있으면 이를 반환한다.
         *
         * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
         *
         * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
         *
         * @param {string} pong  PongResponse 수신받든 Pong 객체
         * @desc Native To JS
         */
        notifyPong(pong: string): void;
        /**
         * 마이크 상태를 반환한다.
         *
         * 1. 마이크 열림 성공, 또는 실패
         * 2. 최초 음성 레코딩이 시작될 때 알림
         * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
         *
         * @param {string } st MicrophoneStatus type
         * @desc Native To JS
         */
        notifyMicrophoneStatus(st: string): void;
        /**
         * 마이크 개방 후 상태를 알려줍니다.
         *
         * @param {string } ss SpeakerStatus 마이크 개방 상태
         * @desc Native To JS
         */
        notifySpeakerStatus(ss: string): void;
        /**
         * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
         *
         * @param {string} cs CameraStatus type 카메라 상태
         * @desc Native to JS
         */
        notifyCameraStatus(cs: string): void;
        /**
         * 갤러리 상태를 반환한다.
         *
         * @param {string} st GalleryStatus type
         */
        notifyGalleryStatus(st: string): void;
        /**
         *
         * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
         */
        notifyLocation(st: string): void;
    }
    /**
     * 네이티브에서 JAVASCRIPT를 호출할 때 사용하는 정보
     *
     * 아래와 같은 인터페이스 구조로 동작합니다.
     */
    export interface M2uWebViewClientCallable {
        /**
         * MAP 서버로부터 응답을 받은다.
         * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
         *
         * @param {DirectiveStream} dir 응답받은 메시지 데이터
         * @desc Native to JS
         */
        receiveDirective(dir: DirectiveStream): void;
        /**
         * map 통신 중에 수신한 exception
         *
         * mapdirective의 내용을 json 형태로 전송한다.
         *
         * @param {MapException} ex MAP 통신 중에서 Exception이 발생한 경우
         * @desc Native to JS
         */
        receiveException(ex: MapException): void;
        /**
         * 에러를 수신한다.
         * grpc Error를 수신한 경우에 이를 담아서 보내준다
         *
         * @param {GrpcStatus} status GRPC 에러 생태 값 및 상세 정보
         * @desc Native to JS
         */
        receiveError(status: GrpcStatus): void;
        /**
         * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
         * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {StreamingStatus} st
         * @desc Native To JS
         */
        sendStreamingEvent(st: StreamingStatus): void;
        /**
         * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
         * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
         * 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {StreamingStatus} st
         * @desc Native To JS
         */
        receiveStreamingDirective(st: StreamingStatus): void;
        /**
         * Ping의 결과가 있으면 이를 반환한다.
         *
         * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
         *
         * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
         *
         * @param {PongResponse} pong 수신받든 Pong 객체
         * @desc Native To JS
         */
        notifyPong(pong: PongResponse): void;
        /**
         * 마이크 상태를 반환한다.
         *
         * 1. 마이크 열림 성공, 또는 실패
         * 2. 최초 음성 레코딩이 시작될 때 알림
         * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
         *
         * @param {MicrophoneStatus} st
         * @desc Native To JS
         */
        notifyMicrophoneStatus(st: MicrophoneStatus): void;
        /**
         * 마이크 개방 후 상태를 알려줍니다.
         *
         * @param {SpeakerStatus} ss 마이크 개방 상태
         * @desc Native To JS
         */
        notifySpeakerStatus(ss: SpeakerStatus): void;
        /**
         * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
         *
         * @param {CameraStatus} cs 카메라 상태
         * @desc Native to JS
         */
        notifyCameraStatus(cs: CameraStatus): void;
        /**
         * 갤러리 상태를 반환한다.
         *
         * @param {GalleryStatus} st
         */
        notifyGalleryStatus(st: GalleryStatus): void;
        /**
         *
         * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
         */
        notifyLocation(st: LocationStatus): void;
    }
    export class AppleM2uWebViewClientNative implements M2uWebViewClientNativeMobile {
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {string} setting, MapSetting 값의 json string 버전
         * @returns {boolean}
         * @desc Js to Native
         */
        setMapSetting(setting: string): boolean;
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
         * @param {string | null} opt 이미지 URL
         * @desc Js to Native
         */
        sendEvent(es: string, opt: string | null): void;
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
         * @desc JS to Native
         */
        openMicrophone(param: string): void;
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        closeMicrophone(): void;
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        closeSpeaker(): void;
        /**
         * 카메라를 엽니다.
         *
         * @param {string} param 문자열로 변환된 CameraParam 객체
         * @desc JS To Native
         */
        openCamera(param: string): void;
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        closeCamera(): void;
        /**
         * 갤러리를 오픈한다.
         * call to native
         * @param {string} param
         */
        openGallery(param: string): void;
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        closeGallery(): void;
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        getLocation(): void;
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        getPhoneNumber(): any;
        constructor();
    }
    /**
     * 디버깅 용도로 사용되는 Local 구현
     */
    export class LocalM2uWebViewClientNative implements M2uWebViewClientNativeMobile {
        /**
         * `CDK Native`에 Map 통신을 위한 기본 설정을 정의한다.
         *  - TLS 사용여부
         *  - 서버의 주소 및 포트
         *  - Ping 주기
         *
         * @param {string} setting, MapSetting 값의 json string 버전
         * @returns {boolean}
         * @desc Js to Native
         */
        setMapSetting(setting: string): boolean;
        /**
         * MapEvent를 보내기 위해서 사용할 이벤트 규격을 정의한다.
         *
         * - streamid
         * - operationid
         * - Param
         * - Payload 데이터는 함께 묶어서 전송한다.
         *
         *
         *  Native CDK의 이 구현부는 다음 사항을 구현해야 한다.
         *
         *  - 해당 메시지를 map 서버로 보낼 때, 일부는 자동으로 메시지를 전송해야 한다.
         *    - STT 음성의 전송
         *    - 이미지 서버로 이미지 전송
         *    - 이런 스트림 형식의 데이터를 보내야 하므로 `StreamEnd`는 직접 처리해야 한다.
         *  - Device 정보는 `setMapSetting`을 통해서 처리한다.
         *    따라서 Device 정보는 EventStream에 포함해서 보내지 않는다.
         *  - 대신 Native는 현재의 시간과 timezone 정보를 수집해서 처리해야 한다.
         *
         * @param {string} es 서버로 보낼 이벤트 메시지, EventStream 객체의 string 버전
         * @param {string | null} opt 이미지 URL
         * @desc Js to Native
         */
        sendEvent(es: string, opt: string | null): void;
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {string} param 개방 옵션, MicrophoneParam 객체를 json 문자열로 변환한 것
         * @desc JS to Native
         */
        openMicrophone(param: string): void;
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        closeMicrophone(): void;
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        closeSpeaker(): void;
        /**
         * 카메라를 엽니다.
         *
         * @param {string} param 문자열로 변환된 CameraParam 객체
         * @desc JS To Native
         */
        openCamera(param: string): void;
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        closeCamera(): void;
        openGallery(param: string): void;
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        closeGallery(): void;
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        getLocation(): void;
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        getPhoneNumber(): any;
        constructor();
    }
}
declare module "main" {
    import { AsyncInterface, AvatarSetExpressionPayload, Card, Device, EventPayload, EventStream, ExpectSpeechPayload, ImageRecognitionParam, LauncherLaunchAppPayload, LauncherAuthorizePayload, LauncherFillSlotsPayload, LauncherLaunchPagePayload, LauncherViewMapPayload, Location, MapException, MultiFactorVerifyPayload, OpenUtter, RenderTextPayload, SignInPayload, SignInResultPayload, SignOutResultPayload, SpeechRecognitionParam, StreamingRecognizeResponse, Struct, UserKey, UserSettings, Utter, DialogAgentForwarderParam } from "proto";
    import { CameraParam, CameraStatus, GalleryParam, GalleryStatus, GrpcStatus, LocationStatus, M2uWebViewClientNative, M2uWebViewClientNativeEventForward, M2uWebViewClientNativeMobile, MapSetting, MicrophoneParam, MicrophoneStatus, SpeakerStatus, StreamingStatus } from "native";
    /**
     * 윈도우 객체에 추가적인 값을 정의합니다.
     */
    global {
        /**
         * 표준 윈도우 객체
         */
        interface Window {
            /**
             * Android Native Interface의 이름
             */
            m2uWebViewNative: M2uWebViewClientNativeMobile;
            /**
             * 네이티브에서 JS로 호출할 때 사용하는 객체의 이름을 담는 중간 객체
             */
            m2uNativeForward: any;
            mySecret: any;
        }
    }
    /**
     * 서버에서 내려온 디렉티브 및 네이티브에서 발생한 이벤트를 묶어서 처리하는 구조
     */
    export interface MapDirectiveHandler {
        /**
         * 텍스트 출력 콜백
         * @param {RenderTextPayload} text RnderText 호출에 따르는 Payload
         */
        onViewRenderText(text: RenderTextPayload): void;
        /**
         * 카드 출력 콜백
         * @param {Card} card 메시지 카드
         */
        onViewRenderCard(card: Card): void;
        /**
         * 메타 데이터 수신 콜백
         * @param {Struct} hidden
         */
        onViewRenderHidden(hidden: Struct): void;
        /**
         * 음성인식 진행 중일 때 콜백
         * @param {StreamingRecognizeResponse} o 음성인식 응답
         */
        onSpeechRecognizerRecognizing(o: StreamingRecognizeResponse): void;
        /**
         * 음성인식 완료되었을 때 콜백
         * @param {StreamingRecognizeResponse} o 음성인식 응답
         */
        onSpeechRecognizerRecognized(o: StreamingRecognizeResponse): void;
        /**
         * 대화 처리 중일 때 콜백
         */
        onDialogProcessing(): void;
        /**
         * 음성합성 출력할 때 콜백
         */
        onSpeechSynthesizerPlayStarted(): void;
        /**
         * 음성 입력 대기 수신할 때 콜백
         * @param {ExpectSpeechPayload} o 음성입력 대기에 필요한 추가적인 Payload 데이터
         */
        onMicrophoneExpectSpeech(o: ExpectSpeechPayload): void;
        /**
         * 아바타 설정 명령어 수신할 때 콜백
         * @param {AvatarSetExpressionPayload} o 아바타 출력을 위한 페이로드
         */
        onAvatarSetExpression(o: AvatarSetExpressionPayload): void;
        /**
         * 인증을 실행하라는 명렁어 수신할때 콜백
         * @param {LauncherAuthorizePayload} o
         * @param {DialogAgentForwarderParam} p
         */
        onLauncherAuthorize(o: LauncherAuthorizePayload, p: DialogAgentForwarderParam): void;
        /**
         * 단말이 가지고 있는 슬롯을 채워서 보내달라는 콜백
         * @param {LauncherFillSlotsPayload} o
         * @param {DialogAgentForwarderParam} p
         */
        onLauncherFillSlots(o: LauncherFillSlotsPayload, p: DialogAgentForwarderParam): void;
        /**
         * 클라이언트에게 LaunchApp을 실행해달라는 콜백
         * @param {LauncherLaunchAppPayload} o
         */
        onLauncherLaunchApp(o: LauncherLaunchAppPayload): void;
        /**
         * 클라이언트에게 LaunchPage을 실행해달라는 콜백
         * @param {LauncherPagePayload} o
         */
        onLauncherLaunchPage(o: LauncherLaunchPagePayload): void;
        /**
         * 클라이언트에게 LauncherViewMap을 실행해달라는 콜백
         * @param {LauncherViewMapPayload} o
         */
        onLauncherViewMap(o: LauncherViewMapPayload): void;
        /**
         * 이미지 인식이 완료되었을 때
         */
        onImageDocumentRecognized(): void;
        /**
         * 로그인에 대한 응답 콜백
         * @param {SignInResultPayload} o 인증 결과 PAYLOAD
         */
        onAuthenticationSignInResult(o: SignInResultPayload): void;
        /**
         * 로그아웃에 대한 응답 콜백
         * @param {SignOutResultPayload} o 응답 메시지
         */
        onAuthenticationSignOutResult(o: SignOutResultPayload): void;
        /**
         * 다중 인증에 대한 응답이 올 때 발생하는 콜백
         * @param {SignInResultPayload} o 인증 결과 PAYLOAD
         */
        onAuthenticationMultiFactorVerifyResult(o: SignInResultPayload): void;
        /**
         * 사용자 속성 정의에 대한 응답이 올 때 발생하는 콜백
         * @param {UserSettings} o 사용자 속성 정의 결과 PAYLOAD
         */
        onAuthenticationUpdateUserSettingsResult(o: UserSettings): void;
        /**
         * 사용자 속성 조회에 대한 응답이 올 때 발생하는 콜백
         * @param {UserSettings} o 사용자 속성 정의 결과 PAYLOAD
         */
        onAuthenticationGetUserSettingsResult(o: UserSettings): void;
        /**
         * 예외가 발생한 경우에 발생할 콜백
         * @param {MapException} e MapException
         */
        onException(e: MapException): void;
        /**
         * 호출이 완료될 때 나올 콜백
         * @param {string} opid 오퍼레이션 ID
         */
        onOperationCompleted(opid: string): void;
        /**
         * 에러가 발생될 때 처리할 콜밸
         * @param {GrpcStatus} error 에러
         */
        onError(error: GrpcStatus): void;
        /**
         * 음성이나 이미지를 전송 중이거나 음성 출력이 내려오고 있을 때
         * 정보를 알려줍니다.
         *
         * @param {StreamingStatus} o
         */
        onStreamingEvent(o: StreamingStatus): void;
        /**
         * 마이크 생태가 바뀔 때 콜백
         * @param {MicrophoneStatus} st 마이크 상태
         */
        onMicrophoneStatus(st: MicrophoneStatus): void;
        /**
         * 마이크 생태가 바뀔 때 콜백
         *
         * @param {SpeakerStatus} st 스피커 상태
         */
        onSpeakerStatus(st: SpeakerStatus): void;
        /**
         * 카메라 상태 정보
         *
         * @param {CameraStatus} st 카메라 상태
         */
        onCameraStatus(st: CameraStatus): void;
        /**
         * 갤러리 상태 정보
         *
         * @param {GalleryStatus} st 갤러리 상태
         */
        onGalleryStatus(st: GalleryStatus): void;
        /**
         * 위치 상태 정보 변경시 콜백
         * @param {LocationStatus} st 위치정보 변경
         */
        onLocationChanged(st: LocationStatus): void;
    }
    /**
     * 대화 인터페이스들을 호출 함수 묶음
     */
    export interface Dialog {
        /**
         * 새로운 세션을 엽니다.
         *
         * M2U 시스템은 세션이 열리자 마자 새로운 발화를 내보내개 됩니다.
         *
         * @param {OpenUtter} openUtter
         *   `openUtter.chatbot`이 비어있으면 Device.
         *   `openUtter.utter`는 기본값이 비어있습니다.
         *   `openUtter.meta`에는 원하는 추가적인 정보를 넣어줄 수도 있습니다.
         * @returns {string} operation id
         */
        open(openUtter: OpenUtter): string;
        /**
         * 음성으로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        speechToSpeechTalk(param?: SpeechRecognitionParam): string;
        /**
         * 음성으로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        speechToTextTalk(param?: SpeechRecognitionParam): string;
        /**
         * 텍스트로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        textToTextTalk(utter: Utter): string;
        /**
         * 텍스트로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         */
        textToSpeechTalk(utter: Utter): string;
        /**
         * 이미지를 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         */
        imageToTextTalk(imageUrl: string, param?: ImageRecognitionParam): string;
        /**
         * 이미지를 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         */
        imageToSpeechTalk(imageUrl: string, param?: ImageRecognitionParam): string;
        /**
         * 명시적으로 세션을 닫습니다.
         */
        close(): string;
    }
    /**
     * 인증 처리 인터페이스
     */
    export interface Authentication {
        /**
         * 인증을 호출합니다.
         *
         * @param {SignInPayload} payload 인증 처리 요청 데이터
         */
        signIn(payload: SignInPayload): void;
        /**
         * SignOut을 호출합니다.
         * 인증토큰을 클리어합니다.
         */
        signOut(): void;
        /**
         * 다중 팩터 인증을 시작합니다.
         *
         * @param {MultiFactorVerifyPayload} payload 다중 팩터 인증을 위한 처리 데이터
         */
        multiFactorVerify(payload: MultiFactorVerifyPayload): void;
        /**
         * 사용자 속성 정의
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 바꾸고 싶은 값만 넣을 수 있다.
         * 응답으로는 모든 설정정보를 다 반환한다.
         *
         * @param {UserSettings} userSettings 사용자 속성 정의 데이터
         * @returns {string}
         * @constructor
         */
        updateUserSettings(payload: UserSettings): string;
        /**
         * 사용자 속성 조회
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 설정정보를 조회한다.
         *
         * @param {UserKey} userKey 사용자 속성 조회를 위한 키
         * @returns {string}
         * @constructor
         */
        getUserSettings(payload: UserKey): string;
    }
    /**
     * Native에서 MAPClient 쪽으로 보내는 메시지를 해석해서 내부적으로 이벤트를 발생시켜주는 클래스
     *
     * 이벤트의 유형에 따라서 다양한 메시지를 전달해준다.
     */
    export class NativeEventForward implements M2uWebViewClientNativeEventForward {
        private mapClient;
        /**
         * NativeEventForward 생성자
         * @param {MapClient} mapClient `mapClient`로부터 받은 이벤트를 처리하도록 한다.
         */
        constructor(mapClient: MapClient);
        /**
         * MAP 서버로부터 응답을 받은다.
         * - opid에 대한 싱크 관리는 Web에서 처리하도록 한다.
         *
         * @param {string} directive 응답받은 메시지 데이터
         * @desc Native to JS
         */
        receiveDirective(directive: string): void;
        /**
         * map 통신 중에 수신한 exception
         *
         * mapdirective의 내용을 json 형태로 전송한다.
         *
         * @param {string} exception MapException MAP 통신 중에서 Exception이 발생한 경우
         * @desc Native to JS
         */
        receiveException(exception: string): void;
        /**
         * 해당 `OPERATION`이 완료되면, 발생시킨다.
         *
         * Java의 경우, `OnComplete()` 호출의 결과 전송하도록 한다.
         *
         * @param {string} opid operation id
         */
        receiveCompleted(opid: string): void;
        /**
         * 에러를 수신한다.
         * grpc Error를 수신한 경우에 이를 담아서 보내준다
         *
         * @param {string} status GrpcStatus GRPC 에러 생태 값 및 상세 정보
         * @desc Native to JS
         */
        receiveError(status: string): void;
        /**
         * SendEvent에 딸린 스트림을 전송하는 경우에는 Web에 알려준다.
         * 스트림을 전송하고 있는 동안에 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus 타입
         * @desc Native To JS
         */
        sendStreamingEvent(st: string): void;
        /**
         * receiveDirective에 딸린 스트림을 수신하는 경우에는 Web에 알려준다.
         * 이것은 TTS 출력 중인 상태와 같이 스트림을 수신하고 있는 동아네
         * 화면에 정보를 표시할 수 있도록 처리한다.
         *
         * 스트림의 끝, 스트림 중지의 경우에도 보내준다
         *
         * 보낸 내용을 제외하고 보낸 바이트를 보내준다.
         *
         * @param {string} st StreamEventStatus
         * @desc Native To JS
         */
        receiveStreamingDirective(st: string): void;
        /**
         * Ping의 결과가 있으면 이를 반환한다.
         *
         * iOS의 경우에 백그라운드 앱은 네트워크를 사용할 수 없으므로 깨어나면 바로 자동으로 시작하도록 한다.
         *
         * setMapSetting() 에 의해서 pingEnabled = false로 주어지면, ping은 내부적으로 중지될 수 있다.
         *
         * @param {string} pong  PongResponse 수신받든 Pong 객체
         * @desc Native To JS
         */
        notifyPong(pong: string): void;
        /**
         * 마이크 상태를 반환한다.
         *
         * 1. 마이크 열림 성공, 또는 실패
         * 2. 최초 음성 레코딩이 시작될 때 알림
         * 3. 마이크 닫는 호출에 대한 응답을 반환한다.
         *
         * @param {string } st MicrophoneStatus type
         * @desc Native To JS
         */
        notifyMicrophoneStatus(st: string): void;
        /**
         * 마이크 개방 후 상태를 알려줍니다.
         *
         * @param {string } ss SpeakerStatus 마이크 개방 상태
         * @desc Native To JS
         */
        notifySpeakerStatus(ss: string): void;
        /**
         * 성공, 실패, 데이터 수집 등 카메라 상태를 반환한다.
         *
         * @param {string} cs CameraStatus type 카메라 상태
         * @desc Native to JS
         */
        notifyCameraStatus(cs: string): void;
        /**
         * 갤러리 상태를 반환한다.
         *
         * @param {string} st GalleryStatus type
         */
        notifyGalleryStatus(st: string): void;
        /**
         * 위치정보가 구해지면 이를 저장합니다.
         *
         * @param {LocationStatus} st 구해진 위치 정보를 반환한다.
         */
        notifyLocation(st: string): void;
    }
    /**
     * 디버거를 통해서 MapClient의 동작을 모니터링 할 수 있는 API 구조
     */
    export interface MapClientDebugger {
        /**
         * Stream DUMP
         * @param {string} dump
         */
        onEvent(dump: string): void;
        /**
         * Directive DUMP
         * @param {string} dump
         */
        onDirective(dump: string): void;
    }
    /**
     * CDK main class
     *
     * 이 클래스를 통해서 모든 함수 호출이 처리된다.
     */
    export class MapClient implements Dialog, Authentication, M2uWebViewClientNative {
        /**
         * Native로 보낸 EventStream이 streaming 형식일 때 기억하기 위한 변수
         * native로 보내고 난후, `notifyEventStream` 호출을 통해서 정보가 오는데, 이때는 streamId 정보만 온다.
         * 어떤 event에 대한 정보인지를 알수가 없으므로 이를 기억한다.
         *
         * @type {Object}
         * @private
         */
        _streams: any;
        /**
         * Native로 보낸 EventStream에 대해서 기억하는 변수
         * 각각 이벤트에 대한 응답이 오면 지운다.
         * @type {Object}
         * @private
         */
        private _operations;
        /**
         * native에서 JS를 호출할 때 사용하는 변수
         * native에서는 `window.m2uNativeForward.[XXXXX].recieveDirective` 와 같은 형식으로 호출한다.
         * 보안 때문에 내부 함수를 바꿔치기할 수 없도록 이름을 랜덤으로 생성한다.
         * @type {string}
         * @private
         */
        private _forwarderName;
        /**
         * Native의 설정값, ip, port 등의 값이 들어있다.
         */
        private _settings;
        /**
         * 실지로 native로 메시지를 전송하는 객체
         * @type {M2uWebViewClientNativeMobile}
         */
        private native;
        /**
         * native의 이벤트를 받아들여서 이를 local WEB javascript callback 함수를 호출해준다.
         * @type {NativeEventForward}
         */
        private forwader;
        /**
         * 현재 위치를 기억하고 있습니다.
         * @type {Location}
         * @private
         */
        private _location;
        /**
         * 등록된 콜백 함수들
         * @type {MapDirectiveHandler}
         * @private
         */
        private _handler;
        /**
         * 등록된 디버깅 함수들
         * @type {MapClientDebugger}
         * @private
         */
        _debug: MapClientDebugger;
        private static suffixLength;
        deviceSuffix: string;
        /**
         * `MapClient`로 새로 만듭니다.
         *
         * @param {MapSetting} settings Map 서버 접속 주소 등의 정보를 가지고 있습니다.
         * @param {string} deviceSuffix 호출을 구분하는 OPERATION ID를 구분하는 정보
         */
        constructor(settings: MapSetting, deviceSuffix: string);
        /**
         * 설정을 가져온다.
         * @returns {MapSetting}
         */
        /**
        * 설정을 바꾼다.
        * @param {MapSetting} value
        */
        settings: MapSetting;
        /**
         * 디바디스 정보를 설정한다.
         * @param {Device} value 변경할 디바이스 정보
         */
        /**
        * 현재 디바이스 상태를 반환한다.
        *
        * @returns {Device} 현재 디바이스 상태
        */
        device: Device;
        /**
         * 인증 토큰을 반환한다.
         *
         * @returns {string}
         */
        /**
        * 인증 토큰을 변경한다.
        * @param {string} value
        */
        authToken: string;
        /**
         * 현재 위치
         * @returns {Location}
         */
        /**
        * 로케이션 지정
        * @param {Location} value
        */
        location: Location;
        /**
         * 현재 설정된 핸들러 반환
         * @returns {MapDirectiveHandler}
         */
        /**
        * 핸들러 지정
        * @param {MapDirectiveHandler} handler
        */
        handler: MapDirectiveHandler;
        /**
         * 인증을 호출합니다.
         *
         * @param {SignInPayload} payload 인증 처리 요청 데이터
         */
        signIn(payload: SignInPayload): void;
        /**
         * SignOut을 호출합니다.
         * 내부에 들고 있는 authToken 정리합니다.
         * @returns {string} operation id
         */
        signOut(): string;
        /**
         * 다중 팩터 인증을 처리합니다.
         *
         * @param {MultiFactorVerifyPayload} payload 다중 팩터 인증을 위한 처리 데이터
         * @returns {string} operation id
         */
        multiFactorVerify(payload: MultiFactorVerifyPayload): string;
        /**
         * 사용자 속성 정의
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 바꾸고 싶은 값만 넣을 수 있다.
         * 응답으로는 모든 설정정보를 다 반환한다.
         *
         * @param {UserSettings} userSettings 사용자 속성 정의 데이터
         * @returns {string}
         * @constructor
         */
        updateUserSettings(payload: UserSettings): string;
        /**
         * 사용자 속성 조회
         * 인증된 사용자만 이 호출을 진행할 수 있다.
         * 설정정보를 조회한다.
         *
         * @param {UserKey} userKey 사용자 속성 조회를 위한 키
         * @returns {string}
         * @constructor
         */
        getUserSettings(payload: UserKey): string;
        /**
         * 새로운 세션을 엽니다.
         *
         * M2U 시스템은 세션이 열리자 마자 새로운 발화를 내보내개 됩니다.
         *
         * @param {OpenUtter} openUtter
         *   `openUtter.chatbot`이 비어있으면 Device.
         *   `openUtter.utter`는 기본값이 비어있습니다.
         *   `openUtter.meta`에는 원하는 추가적인 정보를 넣어줄 수도 있습니다.
         * @returns {string} operation id
         */
        open(openUtter: OpenUtter): string;
        /**
         * 음성으로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        speechToSpeechTalk(param?: SpeechRecognitionParam): string;
        /**
         * 음성으로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * - SpeechRecognizer.Recognizing
         * - SpeechRecognizer.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {SpeechRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
         * @returns {string} operation id
         */
        speechToTextTalk(param?: SpeechRecognitionParam): string;
        /**
         * 텍스트로 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        textToTextTalk(utter: Utter): string;
        /**
         * Client가 명시적으로 interface, operation 명을 지정할 수 있는 디렉티브 생성 함수
         *
         * 예1) MAP으로 부터 Launcher.Authorize 를 수신 받았다면
         * Client는 interface를 Launcher, operation은 Authorized로 설정하여 해당 함수를 호출한다
         * 또는 interface를 Launcher, operation은 AuthorizeFailed로 설정하여 해당 함수를 호출한다
         *
         * 예2) MAP으로 부터 Launcher.FillSlots 를 수신 받았다면
         * Client는 interface를 Launcher, operation은 SlotstFilled로 설정하여 해당 함수를 호출한다
         *
         * @param {string} interfaceName
         * @param {string} operationName
         * @param {EventPayload} eventPayload
         * @param {DialogAgentForwarderParam} param dialog agent forwarder param
         * @returns {string}
         */
        event(interfaceName: string, operationName: string, eventPayload: EventPayload, param: DialogAgentForwarderParam): string;
        /**
         * 텍스트로 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {Utter} utter 이 값은 필수입니다.
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        textToSpeechTalk(utter: Utter): string;
        /**
         * 이미지를 보내고 텍스트로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        imageToTextTalk(imageUrl: string, param?: ImageRecognitionParam): string;
        /**
         * 이미지를 보내고 음성으로 받는 대화를 시작합니다.
         *
         * 이 호출에 대한 대응으로 서버는 다음 응답이벤트를 발생하게 됩니다.
         *
         * 사용자가 직접 타이핑을 한 경우에는 inputType에 `KEYBOARD`를 지정해야 합니다.
         * 만일 선택형 카드에서 답을 한 경우에는 `TOUCH`로 지정합니다.
         *
         * - ImageDocument.Recognized
         * - Dialog.Processing
         * - SpeechSynthesizer.Play
         * - View.RenderText
         * - View.RenderCard
         * - View.RenderHidden
         * - Microphone.ExpectSpeech
         *
         * 위 콜백은 각각 지정된 콜백함수를 통해서 호출되게 됩니다.
         *
         * @param {string} imageUrl 사진 캡춰 이벤트로부터 전달받은 imageUrl을 전달합니다.
         *    이 URL은 네이티브 인터페이스로부터 전달받은 값입니다.
         * @param {ImageRecognitionParam} param 이 값이 없으면 기본 설정으로 처리됩니다.
      
         * @throws Error  없으면 예외가 발생합니다. 'invalid argument'
         * @returns {string} operation id
         */
        imageToSpeechTalk(imageUrl: string, param?: ImageRecognitionParam): string;
        /**
         * 명시적으로 세션을 닫습니다.
         * @returns {string} operation id
         */
        close(): string;
        /**
         * map setting 정보를 넣어준다.
         *
         * @param {MapSetting} setting 변경할 map setting 정보
         * @returns {boolean} 새로운 map setting을 넣어준다.
         */
        setMapSetting(setting: MapSetting): boolean;
        /**
         * 스트립 ID를 정리합니다.
         */
        clearStreamId(strId: string): void;
        /**
         * operation에 대한 정보 삭제
         * @param {string} opid operationId
         */
        clearOperationId(opid: string): void;
        /**
         * operation에 대한 호출 인터페이스 정보를 가져옵니다.
         * @param {string} streamId 인터페이스 ID를 streamID입니다.
         * @return {AsyncInterface}
         */
        getAsyncInterface(streamId: string): AsyncInterface;
        /**
         * 내부적으로 이벤트를 전송합니다.
         *
         * 이 메소드는 직접 호출할 수도 있지만, 다른 wrapper 함수를 통해서 호출하는 것이
         * 바람직합니다.
         *
         * 매 전송시마다 새로운 streamId, operationSyncId를 생성해서 전달합니다.
         * `operationSyncId`는 UUID + deviceSuffix를 사용하여 전달합니다.
         *
         * @param {EventStream} es 전송할 이벤트
         * @param {string} imageUrl 옵셔널 이미지 URL
         */
        sendEvent(es: EventStream, imageUrl?: string): void;
        /**
         * 마이크를 열어 달라고 하는 이벤트
         *
         * @param {MicrophoneParam} param 개방 옵션
         * @desc JS to Native
         */
        openMicrophone(param: MicrophoneParam): void;
        /**
         * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 MIC를 닫는다.
         *
         * @desc JS To Native
         */
        closeMicrophone(): void;
        /**
         * 명시적으로 스피커를 닫는다.
         * @desc JS To Native
         */
        closeSpeaker(): void;
        /**
         * 카메라를 엽니다.
         *
         * @param {CameraParam} param
         * @desc JS To Native
         */
        openCamera(param: CameraParam): void;
        /**
         * 명시적으로 카메라를 닫는다.
         *
         * @desc JS To Native
         */
        closeCamera(): void;
        /**
         * 갤러리를 오픈한다.
         *
         * @param {GalleryParam} param
         */
        openGallery(param: GalleryParam): void;
        /**
         * 명시적으로 Gallery를 닫는다.
         */
        closeGallery(): void;
        /**
         *
         * 매번 현재 단말의 위치를 구해와라.
         *
         * 위치 정보는 JS에서 필요시 호출한다. (예: 주기적으로.. 웹개발자가 알아서..)
         * EventStream의 context 정보에 적재하여 전송한다.
         *
         * @desc JS to Native
         */
        getLocation(): void;
        /**
         *
         * Phone Number 정보는 JS에서 필요시 호출한다.
         *
         * @desc JS to Native
         */
        getPhoneNumber(): any;
    }
    /**
     * JSON.stringify() circular 참조를 호출할 때 오동작을 제거하는 함수
     */
    export function clearReplacer(): void;
    /**
     * JSON.stringify()를 호출할 때, circular 참조를 무시하도록 하는 함수
     *
     * @param key 키
     * @param value 값
     * @return {any} 응답 데이터
     */
    export function replacer(key: any, value: any): any;
}
