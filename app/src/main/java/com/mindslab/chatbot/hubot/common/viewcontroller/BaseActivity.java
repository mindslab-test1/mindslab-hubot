package com.mindslab.chatbot.hubot.common.viewcontroller;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * 권한 체크가 필요한 Activity인 경우 상속받아야 하는 클래스 입니다.
 */

public class BaseActivity extends AppCompatActivity {

  @SuppressWarnings("unused")
  private static final String TAG = BaseActivity.class.getSimpleName();
  private static final int REQUEST_PERMISSION_CALL = 100;
  private static final int REQUEST_PERMISSION_FINE = 101;
  private static final int REQUEST_PERMISSION_STORAGE = 102;
  private static final int REQUEST_PERMISSION_CAMERA = 103;
  private static final int REQUEST_PERMISSION_RECORD_AUDIO = 104;
  private static final int REQUEST_PERMISSION_LIST = 105;
  public static String[] PERMISSIONS_ALL = {
      Manifest.permission.ACCESS_FINE_LOCATION,
      Manifest.permission.WRITE_EXTERNAL_STORAGE,
      Manifest.permission.CAMERA,
      Manifest.permission.RECORD_AUDIO,
      Manifest.permission.CALL_PHONE
  };
  public static String[] PERMISSIONS_RECORD_AUDIO = {
      Manifest.permission.RECORD_AUDIO
  };
  public static String[] PERMISSIONS_GPS = {
      Manifest.permission.ACCESS_FINE_LOCATION,
      Manifest.permission.ACCESS_COARSE_LOCATION
  };
  private static String[] PERMISSIONS_STORAGE = {
      Manifest.permission.WRITE_EXTERNAL_STORAGE
};
private static String[] PERMISSIONS_CAMERA = {
        Manifest.permission.CAMERA
        };
private static String[] PERMISSIONS_CALL = {
        Manifest.permission.CALL_PHONE
        };
protected Context mContext;
  protected String mMyPackageName;
  private List<String> mRequestPermission = new ArrayList<String>();
  private PermissionEventListener mPermissionEventListener;

  /**
   * Acivity를 생성하는 함수입니다.
   */
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  /**
   * 배터리 최적화를 무시했는지 확인하는 함수입니다.
   *
   * @return 배터리 최적화 무시된경우 true
   */
  protected boolean isIgnoreBatteryOptimization() {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
      PowerManager pm =
          (PowerManager) mContext.getSystemService(Context.POWER_SERVICE);
      boolean isIgnoreBattery =
          pm.isIgnoringBatteryOptimizations(mMyPackageName);
      return isIgnoreBattery;
    } else {
      return true;
    }
  }

  /**
   * 권한을 확인하는 함수입니다. (Camera, Audio, Storage 등)
   *
   * @param grantResults 확인할 권한들에 대한 array
   * @return 모든 권한이 granted 된 경우 true
   */
  protected boolean verifyPermissions(int[] grantResults) {
    // At least one result must be checked.
    if (grantResults.length < 1) {
      return false;
    }
    // Verify that each required permission has been granted,
    // otherwise return false.
    for (int result : grantResults) {
      if (result != PackageManager.PERMISSION_GRANTED) {
        return false;
      }
    }
    return true;
  }

  /**
   * 특정 권한에 대해서 Granted 되었는지 확인하는 함수입니다.
   *
   * @param permission Granted 되었는지 확인 할 권한
   * @param listener granted, denied 에 대한 응답을 받을 수 있는 리스너
   */
  protected void checkPermission(String permission,
      PermissionEventListener listener) {
    mPermissionEventListener = listener;
    if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
      checkFinePermission();
    } else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
      checkStoragePermission();
    } else if (permission.equals(Manifest.permission.RECORD_AUDIO)) {
      checkRecordAudioPermission();
    } else if (permission.equals(Manifest.permission.CAMERA)) {
      checkCameraPermission();
    } else if (permission.equals(Manifest.permission.CALL_PHONE)) {
      checkCallPermission();
    }
  }

  /**
   * 특정 권한들에 대해서 Granted 되었는지 확인하는 함수입니다.
   *
   * @param permissions Granted 되었는지 확인 할 권한들
   * @param listener granted, denied 에 대한 응답을 받을 수 있는 리스너
   */
  protected void checkPermissions(String[] permissions,
      PermissionEventListener listener) {
    if (!mRequestPermission.isEmpty()) {
      mRequestPermission.removeAll(mRequestPermission);
    }
    mPermissionEventListener = listener;
    int requestCount = permissions.length;
    boolean isGranted = false;
    for (int i = 0; i < requestCount; i++) {
      String permission = permissions[i];
      if (PermissionChecker.checkSelfPermission(mContext, permission)
          != PackageManager.PERMISSION_GRANTED) {
        mRequestPermission.add(permission);
      }
    }
    String[] sArrays = mRequestPermission
        .toArray(new String[mRequestPermission.size()]);
    if (sArrays.length == 0) {
      mPermissionEventListener.onGrantedPermission();
    } else {
      ActivityCompat.requestPermissions((AppCompatActivity) mContext, sArrays,
          REQUEST_PERMISSION_LIST);
    }
  }

  /**
   * 특정 권한에 대해서 granted 되었는지 확인하는 함수입니다.
   *
   * @param permission granted 되었는지 확인할 권한
   * @return granted 된 경우 true
   */
  protected boolean isGrantedPermission(String permission) {
    if (PermissionChecker.checkSelfPermission(mContext, permission)
        != PackageManager.PERMISSION_GRANTED) {
      return false;
    }
    return true;
  }

  /**
   * Call 관련 접근 권한을 체크하는 함수입니다.
   */
  private void checkCallPermission() {
    if (!isGrantedPermission(Manifest.permission.CALL_PHONE)) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (shouldShowRequestPermissionRationale(
            Manifest.permission.CALL_PHONE)) {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_CALL, REQUEST_PERMISSION_CALL);
        } else {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_CALL, REQUEST_PERMISSION_CALL);
        }
      }
    } else {
      mPermissionEventListener.onGrantedPermission();
    }
  }

  /**
   * 위치정보(GPS) 관련 접근 권한을 체크하는 함수입니다.
   */
  private void checkFinePermission() {
    if (!isGrantedPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (shouldShowRequestPermissionRationale(
            Manifest.permission.ACCESS_FINE_LOCATION)) {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_GPS, REQUEST_PERMISSION_FINE);
        } else {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_GPS, REQUEST_PERMISSION_FINE);
        }
      }
    } else {
      mPermissionEventListener.onGrantedPermission();
    }
  }

  /**
   * Camera 관련 접근 권한을 체크하는 함수입니다.
   */
  private void checkCameraPermission() {
    if (!isGrantedPermission(Manifest.permission.CAMERA)) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_CAMERA, REQUEST_PERMISSION_CAMERA);
        } else {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_CAMERA, REQUEST_PERMISSION_CAMERA);
        }
      }
    } else {
      mPermissionEventListener.onGrantedPermission();
    }
  }

  /**
   * 저장장치 관련 접근 권한을 체크하는 함수입니다.
   */
  private void checkStoragePermission() {
    if (!isGrantedPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (shouldShowRequestPermissionRationale(
            Manifest.permission.READ_PHONE_STATE)) {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_STORAGE, REQUEST_PERMISSION_STORAGE);
        } else {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_STORAGE, REQUEST_PERMISSION_STORAGE);
        }
      }
    } else {
      mPermissionEventListener.onGrantedPermission();
    }
  }

  /**
   * 오디오 관련 접근 권한을 체크하는 함수입니다.
   */
  private void checkRecordAudioPermission() {
    if (!isGrantedPermission(Manifest.permission.RECORD_AUDIO)) {
      if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        if (shouldShowRequestPermissionRationale(
            Manifest.permission.RECORD_AUDIO)) {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_RECORD_AUDIO, REQUEST_PERMISSION_RECORD_AUDIO);
        } else {
          ActivityCompat.requestPermissions((AppCompatActivity) mContext,
              PERMISSIONS_RECORD_AUDIO, REQUEST_PERMISSION_RECORD_AUDIO);
        }
      }
    } else {
      mPermissionEventListener.onGrantedPermission();
    }
  }

  /**
   * 접근 권한에 대한 결과를 알려주는 함수입니다.
   * 설정된 리스너를 통해서 결과를 전달합니다.
   */
  @Override
  public void onRequestPermissionsResult(int requestCode,
                                         String[] permissions, int[] grantResults) {
    if (requestCode == REQUEST_PERMISSION_FINE
        || requestCode == REQUEST_PERMISSION_STORAGE
        || requestCode == REQUEST_PERMISSION_RECORD_AUDIO
        || requestCode == REQUEST_PERMISSION_CAMERA) {
      if (verifyPermissions(grantResults)) {
        mPermissionEventListener.onGrantedPermission();
      } else {
        mPermissionEventListener.onDeniedPermission();
      }
    } else if (requestCode == REQUEST_PERMISSION_LIST) {
      if (verifyPermissions(grantResults)) {
        mPermissionEventListener.onGrantedPermission();
      } else {
        mPermissionEventListener.onDeniedPermission();
      }
    } else {
      super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
  }

  /**
   * 권한에 대한 granted, denied 결과를 전달받기 위한 인터페이스 입니다.
   */
  public interface PermissionEventListener {

    void onGrantedPermission();

    void onDeniedPermission();
  }
}
