package com.mindslab.chatbot.hubot.main.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindslab.chatbot.hubot.R;

public class SideMenuView extends LinearLayout {
    private Context mContext;
    private ImageButton btn_close;
    private TextView tv_exercise, tv_health, tv_disease, tv_etc, tv_setting;


    public SideMenuView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public SideMenuView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View sideMenuView = inflater.inflate(R.layout.view_side_menu, this);

        btn_close = sideMenuView.findViewById(R.id.btn_close);
        tv_exercise = sideMenuView.findViewById(R.id.tv_exercise);
        tv_health = sideMenuView.findViewById(R.id.tv_health);
        tv_disease = sideMenuView.findViewById(R.id.tv_disease);
        tv_etc = sideMenuView.findViewById(R.id.tv_etc);
        tv_setting = sideMenuView.findViewById(R.id.tv_setting);

    }

    public void setOnClickListener(View.OnClickListener listener) {
        btn_close.setOnClickListener(listener);
        tv_exercise.setOnClickListener(listener);
        tv_health.setOnClickListener(listener);
        tv_disease.setOnClickListener(listener);
        tv_etc.setOnClickListener(listener);
        tv_setting.setOnClickListener(listener);
    }
}
