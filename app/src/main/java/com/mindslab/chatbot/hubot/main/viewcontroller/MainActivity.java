package com.mindslab.chatbot.hubot.main.viewcontroller;

import android.Manifest;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.mindslab.chatbot.hubot.R;
import com.mindslab.chatbot.hubot.common.viewcontroller.BaseActivity;
import com.mindslab.chatbot.hubot.main.model.VoChat;
import com.mindslab.chatbot.hubot.main.receiver.JavaScriptReceiver;
import com.mindslab.chatbot.hubot.main.view.SideMenuView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import ai.maum.m2u.cdk.grpclib.agent.DeviceAgentManager;
import ai.maum.m2u.cdk.grpclib.chatbot.GrpcClient;
import ai.maum.m2u.cdk.grpclib.chatbot.common.PreferenceAgent;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapInterfaceName;
import ai.maum.m2u.cdk.grpclib.chatbot.map.MapOperationName;
import ai.maum.m2u.cdk.grpclib.constants.BaseConst;
import ai.maum.m2u.cdk.grpclib.constants.Const;
import ai.maum.m2u.cdk.grpclib.constants.HandlerConst;
import ai.maum.m2u.cdk.grpclib.ui.dto.EtcDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.MapEventDirectiveDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.MicrophoneDTO;
import ai.maum.m2u.cdk.grpclib.ui.dto.StreamingStatusDTO;
import ai.maum.m2u.cdk.grpclib.ui.updater.ExecuteUpdater;
import ai.maum.m2u.cdk.grpclib.ui.updater.ObserverForUpdate;
import ai.maum.m2u.cdk.grpclib.ui.view.ToastView;
import ai.maum.m2u.cdk.grpclib.utils.GsonUtil;
import ai.maum.m2u.cdk.grpclib.utils.LogUtil;
import ai.maum.m2u.cdk.grpclib.utils.MapEventDirectiveParser;
import ai.maum.m2u.cdk.grpclib.utils.StringUtil;
import maum.m2u.map.Map;

public class MainActivity extends BaseActivity implements View.OnClickListener, TextWatcher, ObserverForUpdate {
    private RecyclerView rvChatting;
    private ChatAdapter chatAdapter;
    private EditText etInputText;
    private Button btnSend;
    private ImageButton btnMic, btnMenu;
    private ArrayList<VoChat> chatArrayList;
    private boolean isMicClick = false;
    private boolean isSpeakerPlay = false;
    private boolean isInit = true;
    private WebView webView;
    private DrawerLayout dl_side;
    private SideMenuView view_side_menu;

    // CDK 관련 변수
    private static final String TAG = MainActivity.class.getSimpleName();
    private String mCurrentStreamId = "";
    private String mCurrentSpeechStreamId = "";
    private String mCurrentSpeechOperationSyncId = "";
    private String mCurrentImageStreamId = "";
    private String mCurrentOperationSyncId = "";
    private String mCurrentOperationName = "";
    private int mMultimediaType = Const.MultiMediaType.NONE;

    private JavaScriptReceiver javaScriptReceiver;

    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        mMyPackageName = mContext.getPackageName();

        dl_side = findViewById(R.id.dl_side);
        view_side_menu = findViewById(R.id.view_side_menu);
        webView = findViewById(R.id.webView);
        rvChatting = findViewById(R.id.rvChatting);
        etInputText = findViewById(R.id.etInputText);
        btnSend = findViewById(R.id.btnSend);
        btnMenu = findViewById(R.id.btnMenu);
        btnMic = findViewById(R.id.btnMic);

        init();
    }

    private void init() {
        gson = new Gson();

        ExecuteUpdater updater = ExecuteUpdater.getInstance();
        updater.addObserver(this);

        javaScriptReceiver = new JavaScriptReceiver(this);

        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setTextZoom(100);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.addJavascriptInterface(javaScriptReceiver, "m2uWebViewNative");

        etInputText.addTextChangedListener(this);
        view_side_menu.setOnClickListener(this);
        btnSend.setOnClickListener(this);
        btnMenu.setOnClickListener(this);
        btnMic.setOnClickListener(this);

        chatArrayList = new ArrayList<>();
        chatAdapter = new ChatAdapter(this);
        chatAdapter.setChatList(chatArrayList);

        rvChatting.setAdapter(chatAdapter);
        rvChatting.setLayoutManager(new LinearLayoutManager(this));

        webView.loadUrl("file:///android_asset/index.html");
    }

    private void setChatList(int indicator, String text) {
        VoChat voChat = new VoChat();
        voChat.viewType = indicator;
        voChat.chatString = text;
        chatArrayList.add(voChat);
        chatAdapter.setChatList(chatArrayList);
        rvChatting.scrollToPosition(chatArrayList.size() - 1);
    }

    private boolean isDrawerOpen() {
        return dl_side.isDrawerOpen(Gravity.LEFT);
    }

    private void setFloatingButtonImage(int resId) {
        btnMic.setImageResource(resId);
    }

    @Override
    public void onBackPressed() {
        if (isDrawerOpen()) {
            dl_side.closeDrawer(Gravity.LEFT);
            return;
        }

        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GrpcClient.getInstance(mContext).stopTimerTask(GrpcClient.TIMER_KIND.TIMER_ALL);
        // MAP 과의 통신을 종료시킵니다.
        GrpcClient.getInstance(mContext).shutDownGrcp();
        ExecuteUpdater updater = ExecuteUpdater.getInstance();
        if (updater != null) {
            updater.removeObserver(this);
            updater = null;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable editable) {
        btnSend.setVisibility(View.VISIBLE);
        btnMic.setVisibility(View.VISIBLE);
        btnSend.setTag(editable.toString());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnMenu :
                if (!isDrawerOpen())
                    dl_side.openDrawer(Gravity.LEFT);
                break;

            case R.id.btn_close :
                dl_side.closeDrawer(Gravity.LEFT);
                break;

            case R.id.btnMic:
                if (isSpeakerPlay) {
                    webView.loadUrl("javascript:mapClient.closeSpeaker()");
                    return;
                }

                if (!isMicClick) {
                    webView.loadUrl("javascript:__speechToSpeechTalk()");
                    isMicClick = true;
                }
                etInputText.clearFocus();
                break;

            case R.id.btnSend:
                if (etInputText.getText().toString().length() == 0) {
                    Toast.makeText(this, "입력한 내용이 없습니다.", Toast.LENGTH_SHORT).show();
                    return;
                }
                setChatList(ChatAdapter.VIEW_TYPE_TEXT_USER, String.valueOf(v.getTag()));

                webView.loadUrl("javascript:__textToTextTalk('" + String.valueOf(v.getTag()) + "')");
                etInputText.setText("");

                break;
        }
    }

    @Override
    public void update(int updateCase, String arg1, String arg2, int arg3, boolean arg4, final byte[] arg5, Object obj1, Object obj2) {
        if (isFinishing()) {
            return;
        }

        final String getNotifyObjectPrefix = PreferenceAgent
                .getStringSharedData(this, BaseConst.MapKeys.NOTIFYOBJECT_PREFIX);
        final String notifyObjectPrefix = StringUtil
                .isEmptyString(getNotifyObjectPrefix) ? Const.JAVA_SCRIPT_START :
                Const.JAVA_SCRIPT_START + getNotifyObjectPrefix + ".";

        switch (updateCase) {
            /**
             * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터를
             * Preference 에 저장합니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_JS_SETMAP_SETTING: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String setMapSettingJson = arg1;
                GrpcClient.getInstance(this).setGrpcSettingData(setMapSettingJson);
                LogUtil.e(TAG, "[SETMAP_SETTING]\n" + setMapSettingJson);

                if (isInit) {
                    isInit = false;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            webView.loadUrl("javascript:__signIn()");
                        }
                    });
                }
            }
            break;
            /**
             * JS 에서 sendEvent() 함수를 호출하여 전달한 eventStream 데이터를
             * MAP 에 전송합니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_JS_SENDEVENT: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }

                final String mapEventJson = arg1;
                final String imageUrl = arg2;

                LogUtil.e(TAG, "[UI_FROM_JS_SENDEVENT]\n" + mapEventJson + "\n "
                        + "[imageUrl]\n" + imageUrl);
                // mapEventInfo = mapEventJson 을 parsing 한 데이터
                final MapEventDirectiveDTO.MapEventInfo mapEventInfo = MapEventDirectiveParser.getMapEventInfo(mapEventJson);
                if (mapEventInfo == null) {
                    return;
                }

                mCurrentStreamId = mapEventInfo.getStreamId();
                mCurrentOperationSyncId = mapEventInfo.getOperationSyncId();
                mCurrentOperationName = mapEventInfo.getOperationName();

                if (mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_SPEECHTOTEXTTALK)
                        || mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_SPEECHTOSPEECHTALK)
                        || mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_TEXTTOSPEECHTALK)
                        || mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_TEXTTOTEXTTALK)) {
                    mCurrentSpeechStreamId = mapEventInfo.getStreamId();
                    mCurrentSpeechOperationSyncId = mapEventInfo.getOperationSyncId();
                } else if (mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_IMAGETOTEXTTALK)
                        || mCurrentOperationName.equalsIgnoreCase(MapOperationName.OPERATION_IMAGETOSPEECHTALK)) {
                    mCurrentImageStreamId = mapEventInfo.getStreamId();
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        GrpcClient.getInstance(mContext).grpcSendEvent(mapEventJson);
                        LogUtil.e(TAG, "[SEND-EVENT]\n" + mapEventJson);
                        if (!StringUtil.isEmptyString(imageUrl)) {
                            /*
                             * Camera, Gallery 에서 캡춰한 이미지의 url 을 받은 경우 이 Url 에 대한 image
                             * data 를 MAP 에 전달합니다.
                             * loadImageForSend() 함수에서 getByteParam() 함수를 호출하여 image data 를
                             * byteStream 으로 전송한 후 전송이 끝나면 streamEnd 함수를 호출합니다.
                             */
                            GrpcClient.getInstance(mContext)
                                    .loadImageForSend(imageUrl, mCurrentImageStreamId);
                        } else {
                            /*
                             * STT, STS 의 경우 openMicrophone > sendEvent 순서로 호출되므로
                             * openMicrophone 처리하는 함수에서 Microphone 을 준비하고
                             * 이곳에서 Microphone 을 실행시킵니다.
                             */
                            if (mapEventInfo.getOperationName().equalsIgnoreCase(
                                    MapOperationName.OPERATION_SPEECHTOTEXTTALK)
                                    || mapEventInfo.getOperationName().equalsIgnoreCase(
                                    MapOperationName.OPERATION_SPEECHTOSPEECHTALK)) {
                                GrpcClient.getInstance(mContext).processMicrophoneStart
                                        (mCurrentSpeechStreamId, mCurrentSpeechOperationSyncId);
                                GrpcClient.getInstance(mContext).startTimerForMic();
                            }
                        }
                    }
                });
            }
            break;
            /**
             * JS 에서 setMapSetting() 함수를 호출하여 전달한 MapSetting 데이터에 pingInterval 값이
             * 있으면 이 주기마다 MAP 에 pingRequest 를 호출하고 MAP 으로부터 응답으로 받은 pongResponse 값을
             * JS 에게 전달해 줍니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_MAP_PONG: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String messageJson = arg1;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.e(TAG, "[PONG-UI_FROM_MAP_PONG]\n" + messageJson);
                    }
                });
            }
            break;
            /**
             * JS 에서 openMicrophone() 함수를 호출하여 전달한 MicrophoneDTO.MicrophoneParam
             * 데이터의 설정값을 참고하여 microphone 을 open 시킵니다.
             * 실제 microphone 을 통한 발화는 JS 에서 sendEvent 함수를 호출하여 전달받은
             * SpeechToTextTalk, SpeechToSpeechTalk eventStream 인 경우에 하게 됩니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_JS_OPENMICROPHONE: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String microphoneParamJson = arg1;
                LogUtil.e(TAG, "[OPENMICROPHONE]\n" + microphoneParamJson);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        MicrophoneDTO.MicrophoneParam microPhoneParam = gson.fromJson
                                (microphoneParamJson, MicrophoneDTO.MicrophoneParam.class);
                        processMicrophone(true, microPhoneParam);
                    }
                });
            }
            break;
            /**
             * JS 에서 closeMicrophone() 함수를 호출하면 microphone 을 닫습니다.
             * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 microphone 을 닫을때 closeMicrophone()
             * 함수가 호출될 수 있습니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_JS_CLOSEMICROPHONE: {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processMicrophone(false, null);
                    }
                });
            }
            break;
            /**
             * 현재 microphone 의 상태를 JS 에게 전달합니다.
             * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
             */
            case HandlerConst.UIUpdater.UI_TO_JS_MICROPHONESTATUS: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String microphoneStatusJson = arg1;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.e(TAG, "[MICROPHONE-NOTIFY]\n" + microphoneStatusJson);
                        MicrophoneDTO.MicrophoneStatus microPhoneStatus = gson.fromJson(microphoneStatusJson, MicrophoneDTO.MicrophoneStatus.class);
                        if (MicrophoneDTO.MicrophoneEvent.OPEN_SUCCESS.equals(microPhoneStatus.getEventType())) {
                            webView.loadUrl(notifyObjectPrefix + "notifyMicrophoneStatus('" + microphoneStatusJson + "')");
                            Glide.with(mContext).load(R.drawable.ic_mic_on).centerCrop().into(btnMic);
                            btnSend.setVisibility(View.INVISIBLE);
                        } else if (MicrophoneDTO.MicrophoneEvent.CLOSE_SUCCESS.equals(microPhoneStatus.getEventType())) {
                            setFloatingButtonImage(R.drawable.ic_mic);
                            btnSend.setVisibility(View.VISIBLE);
                            isMicClick = false;
                        } else if (MicrophoneDTO.MicrophoneEvent.CLOSE_FAILURE.equals(microPhoneStatus.getEventType())
                                || MicrophoneDTO.MicrophoneEvent.OPEN_FAILURE.equals(microPhoneStatus.getEventType())
                                || MicrophoneDTO.MicrophoneEvent.UNKNOWN.equals(microPhoneStatus.getEventType())) {
                            setChatList(ChatAdapter.VIEW_TYPE_TEXT_AI, microPhoneStatus.getFailure());
                        }
                    }
                });
            }
            break;
            /**
             * microphone 으로 발화가 시작된 경우(recoding) STARTING_RECORD 이벤트를 JS 에게 전달합니다.
             * MicrophoneDTO.MicrophoneStatus 객체로 구성하여 JSON String 으로 전달합니다.
             */
            case HandlerConst.UIUpdater.UI_TO_JS_MICROPHONE_RECORDINGSTATUS: {
                final boolean recordingStart = arg4;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (recordingStart) {
                            LogUtil.e(TAG, "[MICROPHONE-RECORDING START]\n");
                        } else {
                            LogUtil.e(TAG, "[MICROPHONE-RECORDING STOP]\n");
                        }
                        boolean isResultStatus = Const.RESULT_BOOL_SUCCESS;
                        if (!recordingStart) {
                            isResultStatus = Const.RESULT_BOOL_FAIL;
                        }
                        String strReason = Const.EMPTY_STRING;
                        if (!recordingStart) {
                            strReason = MicrophoneDTO.MicrophoneFailure.MICROPHONE_ERROR_UNKNOWN;
                        }
                        // 레코딩 시작 상태인 경우
                        GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus(
                                MicrophoneDTO.MicrophoneEvent.STARTING_RECORD, Const.EXPECT_MDOE,
                                Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                                Const.EMPTY_STRING);
                    }
                });
            }
            break;
            /**
             * microphone 을 통해 발화한 데이터를 MAP 에 전송하고, JS 에게 관련 정보를 전달한다.
             * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
             */
            case HandlerConst.UIUpdater.UI_TO_JS_STREAMSENT: {
                if (StringUtil.isEmptyString(arg1) || StringUtil.isEmptyString(arg2)) {
                    return;
                }

                final String streamId = arg1;
                final String operationSyncId = arg2;
                final boolean isBreak = (Boolean) obj1;
                final boolean isEnd = (Boolean) obj2;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // 마이크가 릴리즈된 경우 버퍼에 남아있는 byteStream이 있지만
                        // streamEnd를 보낸 경우이므로 전송하지 않는다.
                        if (!DeviceAgentManager.getInstance(mContext).isAudioRecorderObject()) {
                            LogUtil.e(TAG, "UI_TO_JS_STREAMSENT : "
                                    + "Voice Recorder Agent is null...so, not send stream");
                            return;
                        } else {
                            LogUtil.e(TAG, "UI_TO_JS_STREAMSENT : "
                                    + "Voice Recorder Agent is not null...so, send stream");
                        }
                        // sendStreamingEvent() 함수로 StreamingObject 데이터 전송
                        StreamingStatusDTO streamStatusDto = new StreamingStatusDTO();
                        // 스트림 ID
                        streamStatusDto.setStreamId(streamId);
                        // 응답 동기화 ID
                        streamStatusDto.setOperationSyncId(operationSyncId);
                        // 이벤트가 강제로 중지되었는가?
                        streamStatusDto.setBreak(isBreak);
                        // 이벤트의 끝인가?
                        streamStatusDto.setEnd(isEnd);
                        // 스트리밍 데이터의 구체적인 형식
                        streamStatusDto.setType(EtcDTO.Streamingtype.BYTES);
                        // 스트리밍의 크기
                        final byte[] utterVoice = (arg5 != null) ? arg5.clone() : null;

                        streamStatusDto.setSize(utterVoice != null ? utterVoice.length : 0);
                        String streamObjectJson = GsonUtil
                                .jsonStringFromObject(streamStatusDto);
                        LogUtil.e(TAG, "[MICROPHONE_STREAMSENT]\n" + streamObjectJson);

                        // StreamingStatus
                        webView.loadUrl(notifyObjectPrefix + "sendStreamingEvent('" +
                                streamObjectJson + "')");
                        // Microphone으로 발화된 음성을 MAP 에 전달
                        if (utterVoice != null) {
                            GrpcClient.getInstance(mContext)
                                    .processMicrophoneUtterData(utterVoice);
                        }
                    }
                });
            }
            break;
            /**
             * MAP 으로 부터 byteStream, streamEnd 을 수신한 경우
             * JS 에게 byteStream 에 대한 정보를 전달해 줍니다.
             * StreamingStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
             */
            case HandlerConst.UIUpdater.UI_TO_JS_RECEIVESTREAM: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String receiveStreamObjectJson = arg1;
                LogUtil.e(TAG, "[RECEIVESTREAM_DIRECTIVE]\n" + receiveStreamObjectJson);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                    }
                });
            }
            break;

            /**
             * JS 에서 closeSpeaker() 함수를 호출하면 speaker 을 닫습니다.
             * 사용자에 의해서 또는 프로그램 코드에 의해서 명시적으로 speaker 을 닫을때 closeSpeaker() 함수가 호출될
             * 수 있습니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_JS_CLOSESPEAKER: {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        GrpcClient.getInstance(mContext).speakerClose();
                        isSpeakerPlay = false;
                        setFloatingButtonImage(R.drawable.ic_mic);
                    }
                });
            }
            break;
            /**
             * 현재 speaker 의 상태를 JS 에게 전달합니다.
             * SpeakerDTO.SpeakerStatus 객체로 구성하여 JSON String 으로 전달합니다.
             */
            case HandlerConst.UIUpdater.UI_TO_JS_SPEAKERSTATUS: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String speakerStatusJson = arg1;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.e(TAG, "[SPEAKER-NOTIFY]\n" + speakerStatusJson);
                    }
                });
            }
            break;


            /**
             * MAP 에게 eventStream 을 전송하고 MAP 으로 부터 수신이 완료된 경우
             * JS 에게 전송된 operationSyncId 값을 전달한다.
             */
            case HandlerConst.UIUpdater.UI_FROM_MAP_ONCOMPLETED: {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        LogUtil.e(TAG, "[MAP_ONCOMPLETED]\n\n" + mCurrentOperationSyncId);
                    }
                });
            }
            break;
            /**
             * MAP 과 통신중에 gRPC 에러를 수신한 경우에 에러에 대한 정보를
             * GrpcStatusDTO 객체로 구성하여 JSON String 으로 전달합니다.
             * 현재는 Throwable 메시지를 전달하고 있고,
             * 상세 에러 내역을 추출하는 방법을 제공받은 후 그에 대한 수정이 필요합니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_MAP_ONERROR:
            case HandlerConst.UIUpdater.UI_FROM_MAP_ERROR: {
                if (StringUtil.isEmptyString(arg1)) {
                    return;
                }
                final String errorMessageJson = arg1;

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastView.getInstance(mContext)
                                .setToastMsg("[ERROR]\n" + errorMessageJson);
                    }
                });
            }
            break;
            /**
             * JS 에서 호출된 sendEvent() 함수를 통해 전달받은 eventStream 데이터를 MAP 에게 요청하고
             * 이 요청에 대한 응답(MapDirective)을 받은 경우 directive 종류에 따라 처리를 하고
             * directiveStream 인 경우 JS 에게 전달해 줍니다.
             */
            case HandlerConst.UIUpdater.UI_FROM_MAP_ONNEXT: {
                if (obj1 == null) {
                    return;
                }
                final Map.MapDirective mapDirective = (Map.MapDirective) obj1;
                final MapEventDirectiveDTO.MapDirectiveInfo directiveInfo = MapEventDirectiveParser
                        .getMapDirectiveInfo(mapDirective);
                if (directiveInfo == null) {
                    return;
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /**
                         * Directive 로 MapException 이 들어오면 receiveException() 함수를 호출하여
                         * MapException 정보를 전달해 줍니다.
                         */
                        if (directiveInfo.isMapException()) {
                            LogUtil.e(TAG, "[MAPEXCEPTION_ERROR]\n" + directiveInfo
                                    .getMapExceptionJson());
                            return;
                        }
                        /**
                         * MAP 으로 부터 byteStream 를 수신하면 이 데이터의 정보를 JS 에게 전달하고
                         * AudioTrack으로 수신한 byteStream 을 재생합니다.
                         */
                        else if (directiveInfo.isByteStream() || directiveInfo
                                .isByteTextStream()) {
                            byte[] byteData = null;
                            if (directiveInfo.isByteStream()) {
                                byteData = directiveInfo.getByteStream().toByteArray();
                            } else {
                                byteData = directiveInfo.getByteTextStream().toByteArray();
                            }
                            GrpcClient.getInstance(mContext)
                                    .processReceiveStreamDirective(byteData,
                                            mCurrentStreamId, mCurrentOperationSyncId, false, false);
                            // Audio play (PCM)
                            GrpcClient.getInstance(mContext).speakerPlay(byteData);

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isSpeakerPlay = true;
                                    setFloatingButtonImage(R.drawable.ic_speaker_off);
                                }
                            });
                            return;
                        }
                        /**
                         * MAP 으로 부터 streamEnd 를 수신하면 streamEnd 에 대한 정보를
                         * StreamingStatusDTO 객체로 구성한 JSON String 을 JS 에게 전달합니다.
                         */
                        else if (directiveInfo.isStreamEnd()) {
                            // Audio stop (PCM)
                            GrpcClient.getInstance(mContext)
                                    .processReceiveStreamDirective(null,
                                            mCurrentStreamId, mCurrentOperationSyncId, false, true);
                            return;
                        }
                        /**
                         * MAP 으로 부터 받은 MapDirective 가 directiveStream 인 경우 JS 에게 전달한다.
                         */
                        else if (directiveInfo.isDirectiveStream()) {
                            MapEventDirectiveDTO.DirectiveStreamInfo dsInfo = directiveInfo
                                    .getDirectiveStreamInfo();
                            // dsJson : directiveStream of JSON type string
                            String dsJson = directiveInfo.getDirectiveStreamJson();
                            if (dsJson.isEmpty() || dsInfo == null) {
                                LogUtil.e(TAG, "[ONNEXT] directiveStreamInfo is null.");
                                return;
                            } else {
                                LogUtil.e(TAG, "[RECEIVE_DIRECTIVE]\n" + dsJson);
                            }


                            String interfaceName = dsInfo.getInterfaceName();
                            String operationName = dsInfo.getOperationName();
                            String streamId = dsInfo.getStreamId();
                            String operationSyncId = dsInfo.getOperationSyncId();
                            if (interfaceName.equalsIgnoreCase((MapInterfaceName.INTERFACE_SPEECHRECOGNIZER))) {
                                if (operationName.equalsIgnoreCase(MapOperationName.OPERATION_RECOGNIZED)
                                        || operationName.equalsIgnoreCase(MapOperationName.OPERATION_ENDPOINTDETECTED)) {
                                    /*
                                     * MAP 으로 부터 Recognized 수신 시 또는 MAP 으로 부터 EndpointDetected
                                     * 수신 시 VoiceRecorderAgent 를 stop 시키고 streamEnd 호출합니다.
                                     */
                                    GrpcClient.getInstance(mContext).microphoneRelese(true, MicrophoneDTO.MicrophoneEvent.CLOSE_SUCCESS, null);
                                    LogUtil.e(TAG, "[RECEIVE_DIRECTIVE : STREAMEND]\n" + "streamId = " + mCurrentSpeechStreamId);
                                    // Recognized, EndpointDetected 수신 시 streamEnd 호출한다.
                                    GrpcClient.getInstance(mContext).callRequestStreamEnd(mCurrentSpeechStreamId);

                                    setChatList(ChatAdapter.VIEW_TYPE_TEXT_USER, mapDirective.getDirective().getPayload().getFieldsMap().get("results").getListValue().getValues(0).getStructValue().getFieldsMap().get("transcript").getStringValue().replaceAll("\n", ""));

                                } else if (operationName.equalsIgnoreCase(MapOperationName.OPERATION_RECOGNIZING)) {
                                    /**
                                     * MicrophoneParam : timeoutInMillisecond 시간만큼 Timer를 연장합니다.
                                     * (startTimerForMic)
                                     */
                                    GrpcClient.getInstance(mContext).startTimerForMic();
                                }
                            } else if (operationName.equalsIgnoreCase(MapOperationName
                                    .OPERATION_PLAY)) {
                                if (interfaceName.equalsIgnoreCase(MapInterfaceName
                                        .INTERFACE_SPEECHSYNTHESIZER)) {
                                    /*
                                     * SpeechSynthesizer, AudioPlay / Play 인 경우 param /
                                     * speechSynthesizerParam 에 있는 encoding, sampleRate 를
                                     * preference에 저장합니다.
                                     */
                                    try {
                                        JSONObject directiveJsonObject = new JSONObject(dsJson);
                                        String paramValue = directiveJsonObject
                                                .getString("param");
                                        if (!StringUtil.isEmptyString(paramValue)) {
                                            JSONObject paramValueObject = new JSONObject(paramValue);
                                            String speechSynthesizerParamValue = paramValueObject
                                                    .getString("speechSynthesizerParam");
                                            JSONObject speechSynthesizerParamObject = new JSONObject
                                                    (speechSynthesizerParamValue);
                                            String encoding = speechSynthesizerParamObject
                                                    .getString("encoding");
                                            int sampleRate = speechSynthesizerParamObject.getInt
                                                    ("sampleRate");
                                            PreferenceAgent.setStringSharedData(mContext, BaseConst.MapKeys
                                                    .SPEECH_ENCODING, encoding);
                                            PreferenceAgent.setIntSharedData(mContext, BaseConst.MapKeys
                                                    .SPEECH_SAMPLERATE, sampleRate);
                                        }
                                    } catch (JSONException e) {
                                        LogUtil.d(TAG, "[UI_FROM_JS_SENDEVENT]\n" + e.getMessage());
                                        return;
                                    }
                                }
                                /*
                                 * MAP 으로 부터 Play 수신 시 player 를 준비 시키고 이후에 directiveStream 으로
                                 * byteStream 이 수신되면 이 데이터를 AudioTrack 으로 재생합니다.
                                 */
                                GrpcClient.getInstance(mContext).speakerPlayInit();
                            }
                            /**
                             * ExpectSpeech를 받은 경우 directStream을 JS 에게 전달해 주고 JS 는
                             * openMicrophone(param) 을 호출해서 음성인식 처리를 유도합니다.
                             * 여기에서는 따로 처리하지 않습니다.
                             */
                            else if (operationName.equalsIgnoreCase(MapOperationName
                                    .OPERATION_EXPECTSPEECH)) {
                                mCurrentSpeechStreamId = streamId;
                                mCurrentSpeechOperationSyncId = operationSyncId;
                                // 여기에서는 따로 처리하지 않습니다.
                            }
                            /**
                             * MAP 으로부터 수신한 directiveStream 이 ExpectSpeechTimedOut 이면 음성인식
                             * 실패한 경우이며, 이때 마이크가 열려있으면 닫습니다.
                             */
                            else if (operationName.equalsIgnoreCase(MapOperationName
                                    .OPERATION_EXPECTSPEECHTIMEDOUT)) {
                                processMicrophone(false, null);
                            }

                            else if (operationName.equalsIgnoreCase(MapOperationName.OPERATION_RENDERTEXT)) {
                                setChatList(ChatAdapter.VIEW_TYPE_TEXT_AI, mapDirective.getDirective().getPayload().getFieldsMap().get("text").getStringValue());
                            }

                            // directiveStream 인 경우 JS 에게 전달한다.
                            String url = notifyObjectPrefix + "receiveDirective('" + dsJson + "')";
                            webView.loadUrl(url);
                        }
                    }
                });
            }
            break;
            // arg3 : broadcast receiver 에서 수신한 call status 에 대한 처리를 합니다.
            case HandlerConst.UIUpdater.UI_FROM_ETC_CALLSTATUS: {
                final int callState = arg3;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (callState == TelephonyManager.CALL_STATE_RINGING) {
                            GrpcClient.getInstance(mContext)
                                    .processMicrophoneStop(MicrophoneDTO.MicrophoneEvent.CLOSE_SUCCESS,
                                            MicrophoneDTO.MicrophoneFailure.MICROPHONE_UNDER_USE);
                        }
                    }
                });
            }
            break;
        }
    }

    /**
     * JS 로 부터 openMicrophone 함수가 호출된 경우 Microphone 을 open 하고, JS 로 부터
     * closeMicrophone 함수가 호출되었거나 MAP 으로 부터 "ExpectSpeechTimedOut"을 수신한 경우
     * Microphone 을 close 하는 함수입니다.
     * <p>
     * open 액션인 경우 MicrophoneParam 값으로 Microphone 을 초기화 한 후
     * notifyMicrophoneStatus 함수를 통해서 JS 에게 현재 상태를 전달해 줍니다.<br>
     * open 실패나 close 에 대한 상태도 JS 에게 전달해 줍니다.
     * </p>
     *
     * @param isOpen true 인 경우 open, false 인 경우 close
     * @param microPhoneParam true 인 경우 JS 로 부터 전달 받은 MicrophoneParam 값
     */

    private void processMicrophone(boolean isOpen,
                                   final MicrophoneDTO.MicrophoneParam microPhoneParam) {
        // Microphone 열기
        if (isOpen) {
            if (microPhoneParam != null) {
                String errorMessage = "";

                checkPermission(Manifest.permission.RECORD_AUDIO,
                        new PermissionEventListener() {
                            @Override
                            public void onGrantedPermission() {
                                boolean isResult = GrpcClient.getInstance(mContext)
                                        .processMicrophoneInit(microPhoneParam);
                                if (isResult) {
                                    // 마이크 Open 성공
                                    GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                                            (MicrophoneDTO.MicrophoneEvent.OPEN_SUCCESS, Const.EXPECT_MDOE,
                                                    Const.HAS_NOT_REASON, Const.EMPTY_STRING,
                                                    Const.EMPTY_STRING);
                                } else {
                                    // 마이크 Open 실패
                                    GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                                            (MicrophoneDTO.MicrophoneEvent.OPEN_FAILURE, Const.EXPECT_MDOE,
                                                    Const.HAS_REASON, MicrophoneDTO.MicrophoneFailure
                                                            .MICROPHONE_ERROR_UNKNOWN, Const.EMPTY_STRING);
                                }
                            }

                            @Override
                            public void onDeniedPermission() {
                                // 마이크 Open 실패
                                GrpcClient.getInstance(mContext).processNotifyMicrophoneStatus
                                        (MicrophoneDTO.MicrophoneEvent.OPEN_FAILURE, Const.EXPECT_MDOE,
                                                Const.HAS_REASON, MicrophoneDTO.MicrophoneFailure
                                                        .MICROPHONE_PERMISSION_DENIED, Const.EMPTY_STRING);
                            }
                        });
            } else {
                GrpcClient.getInstance(this)
                        .microphoneRelese(false, MicrophoneDTO.MicrophoneEvent.OPEN_FAILURE,
                                MicrophoneDTO.MicrophoneFailure.MICROPHONE_OPEN_ERROR);
            }
        }
        // Microphone 명시적으로 닫기
        else {
            GrpcClient.getInstance(this)
                    .microphoneRelese(true, MicrophoneDTO.MicrophoneEvent.CLOSE_SUCCESS, null);
        }
    }

}
