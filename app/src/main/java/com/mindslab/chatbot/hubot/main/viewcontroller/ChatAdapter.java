package com.mindslab.chatbot.hubot.main.viewcontroller;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mindslab.chatbot.hubot.R;
import com.mindslab.chatbot.hubot.main.model.VoChat;
import com.mindslab.chatbot.hubot.main.view.ChatViewHolder;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatViewHolder> {
    private Context mContext;
    private ArrayList<VoChat> chatList;

    public static final int VIEW_TYPE_TEXT_USER = 0;
    public static final int VIEW_TYPE_TEXT_AI = 1;

    public ChatAdapter(Context context) {
        mContext = context;
        chatList = new ArrayList<>();
    }

    public void setChatList(ArrayList<VoChat> list) {
        chatList = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null;

        switch (viewType) {
            case VIEW_TYPE_TEXT_USER:
                view = layoutInflater.inflate(R.layout.item_chat_user, parent, false);
                break;

            case VIEW_TYPE_TEXT_AI:
                view = layoutInflater.inflate(R.layout.item_chat_ai, parent, false);
                break;
        }

        return new ChatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChatViewHolder chatViewHolder, int position) {
        chatViewHolder.tvChat.setText(chatList.get(position).chatString);
    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }

    @Override
    public int getItemViewType(int position) {
       return chatList.get(position).viewType;
    }
}
